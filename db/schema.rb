# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150823153048) do

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "icons", force: true do |t|
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.string   "un"
    t.string   "name"
    t.string   "filename"
    t.integer  "width"
    t.integer  "height"
    t.boolean  "required"
    t.string   "alt_un"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "icons", ["un"], name: "index_icons_on_un", unique: true, using: :btree

  create_table "node_relations", force: true do |t|
    t.string   "name",           default: "default", null: false
    t.integer  "node_id"
    t.integer  "relation_id"
    t.integer  "weight",         default: 1
    t.integer  "inverse_weight", default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "node_relations", ["name", "node_id", "weight"], name: "index_node_relations_on_name_and_node_id_and_weight", using: :btree
  add_index "node_relations", ["name", "relation_id", "inverse_weight"], name: "inverse_idx", using: :btree

  create_table "nodes", force: true do |t|
    t.integer  "nodeable_id"
    t.string   "nodeable_type"
    t.integer  "node_id"
    t.integer  "reference_node_id"
    t.string   "kind",                    default: "item", null: false
    t.string   "un"
    t.string   "name"
    t.string   "path"
    t.string   "slug"
    t.string   "title"
    t.text     "description"
    t.boolean  "image_required",          default: false,  null: false
    t.integer  "width",                   default: 0,      null: false
    t.integer  "height",                  default: 0,      null: false
    t.boolean  "image_max",               default: false,  null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "image_alt"
    t.boolean  "attachment_required",     default: false,  null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "attachment_type"
    t.string   "link_url"
    t.string   "link_text"
    t.boolean  "link_new_window",         default: true,   null: false
    t.integer  "template"
    t.date     "node_date"
    t.string   "email"
    t.boolean  "active",                  default: true
    t.integer  "weight",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "nodes", ["node_id", "active", "weight"], name: "index_nodes_on_node_id_and_active_and_weight", using: :btree
  add_index "nodes", ["node_id", "kind"], name: "index_nodes_on_node_id_and_kind", using: :btree
  add_index "nodes", ["node_id", "node_date"], name: "index_nodes_on_node_id_and_node_date", using: :btree
  add_index "nodes", ["node_id", "slug"], name: "index_nodes_on_node_id_and_slug", unique: true, using: :btree
  add_index "nodes", ["node_id", "un"], name: "index_nodes_on_node_id_and_un", using: :btree
  add_index "nodes", ["nodeable_id", "nodeable_type"], name: "index_nodes_on_nodeable_id_and_nodeable_type", using: :btree
  add_index "nodes", ["path"], name: "index_nodes_on_path", unique: true, using: :btree
  add_index "nodes", ["reference_node_id"], name: "index_nodes_on_reference_node_id", using: :btree

  create_table "pages", force: true do |t|
    t.integer  "node_id",                          null: false
    t.string   "browser_title"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.string   "sm_frequency"
    t.float    "sm_priority",           limit: 24
    t.string   "fb_title"
    t.text     "fb_desc"
    t.string   "fb_image_file_name"
    t.string   "fb_image_content_type"
    t.integer  "fb_image_file_size"
    t.datetime "fb_image_updated_at"
    t.text     "tw_desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["node_id"], name: "index_pages_on_node_id", unique: true, using: :btree

  create_table "settings", force: true do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username",               default: "", null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end

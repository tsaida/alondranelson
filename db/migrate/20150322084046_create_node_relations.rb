class CreateNodeRelations < ActiveRecord::Migration
  def change
    create_table :node_relations do |t|
      t.string :name, null: false, default: "default"
      t.integer :node_id
      t.integer :relation_id
      t.integer :weight, default: 1
      t.integer :inverse_weight, default: 1

      t.timestamps

      t.index [:name, :node_id, :weight]
      t.index [:name, :relation_id, :inverse_weight], name: "inverse_idx"
    end
  end
end

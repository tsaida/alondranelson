class CreateAdminHub < ActiveRecord::Migration
  def up
    create_table :nodes do |t|
      t.references :nodeable, :polymorphic => true

      # parent node id
      t.integer :node_id
      t.integer :reference_node_id

      # identifiers
      t.string :kind, null: false, default: 'item'
      t.string :un
      t.string :name
      t.string :path
      t.string :slug

      # title and description
      t.string :title
      t.text :description

      # image
      t.boolean :image_required, null: false, default: false
      t.integer :width, null: false, default: 0
      t.integer :height, null: false, default: 0
      t.boolean :image_max, null: false, default: false
      t.attachment :image
      t.string :image_alt

      # attachment
      t.boolean :attachment_required, null: false, default: false
      t.attachment :attachment
      t.string :attachment_type  # pdf, music, video, ...

      # link
      t.string :link_url
      t.string :link_text
      t.boolean :link_new_window, null: false, default: true

      # misc
      t.integer :template
      t.date :node_date
      t.string :email

      # active and weight
      t.boolean :active, default: true
      t.integer :weight, default: 1

      # user and timestamp
      t.timestamps

      # indexes
      t.index :reference_node_id
      t.index [:path], unique: true
      t.index [:node_id, :un]
      t.index [:node_id, :kind]
      t.index [:node_id, :active, :weight]
      t.index [:node_id, :node_date]
      t.index [:node_id, :slug], unique: true
      t.index [:nodeable_id, :nodeable_type]
    end

    create_table :pages do |t|
      t.integer :node_id, null: false

      # sitemap
      t.string :browser_title
      t.text :meta_description
      t.text :meta_keywords
      t.string :sm_frequency #, default: "daily"
      t.float :sm_priority #, default: 0.5

      # social
      t.string :fb_title
      t.text :fb_desc
      t.attachment :fb_image
      t.text :tw_desc

      # indexes
      t.index :node_id, unique: true

      t.timestamps
    end

    create_table :icons do |t|
      t.attachment :icon

      t.string :un
      t.string :name
      t.string :filename
      t.integer :width
      t.integer :height
      t.boolean :required
      t.string :alt_un

      t.timestamps

      t.index [:un], unique: true
    end
  end

  def down
    drop_table :nodes
    drop_table :pages
    drop_table :icons
  end
end

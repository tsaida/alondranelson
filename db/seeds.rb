
def create_icon(un, filename, width, height, name, required=false, alt_un=nil)
  unless Icon.find_by_un(un)
    puts "Seeding icon #{un}"
    Icon.create(un: un, name: name, filename: filename, width: width, height: height, required: required, alt_un: alt_un)
  end
end

if User.count == 0
  User.create!(username: 'admin', email: 'admin@admin.com', password: 'password', password_confirmation: 'password')
end

Site.create!

create_icon("favicon", "favicon.ico", 16, 16, "Favicon", true)
create_icon("apple_touch_icon", "apple-touch-icon.png", 152, 152, "Apple Touch Icon", true)
create_icon("apple_touch_icon_76", "apple-touch-icon-76x76.png", 76, 76, "Apple Touch Icon")
create_icon("apple_touch_icon_120", "apple-touch-icon-120x120.png", 120, 120, "Apple Touch Icon")
create_icon("apple_touch_icon_152", "apple-touch-icon-152x152.png", 152, 152, "Apple Touch Icon")
create_icon("apple_touch_icon_precomposed", "apple-touch-icon-precomposed.png", 152, 152, "Apple Touch Icon Precomposed")
create_icon("apple_touch_icon_76_precomposed", "apple-touch-icon-76x76-precomposed.png", 76, 76, "Apple Touch Icon Precomposed")
create_icon("apple_touch_icon_120_precomposed", "apple-touch-icon-120x120-precomposed.png", 120, 120, "Apple Touch Icon Precomposed")
create_icon("apple_touch_icon_152_precomposed", "apple-touch-icon-152x152-precomposed.png", 152, 152, "Apple Touch Icon Precomposed")

# Symlink icons
Icon.where.not(icon_file_name: nil).each do |item|
  if File.exists?(item.icon.path)
    ln_path = Rails.root.join('public', item.filename)
    FileUtils.rm_rf(ln_path) if File.exists?(ln_path) # delete the file if it exists (favicon.ico)
    FileUtils.ln_sf(item.icon.path, ln_path)
  end
end

# update event images
Node.transaction { Node.where(kind: :event).each { |event| event.update_attributes(height: 0) unless event.height == 0 } }
class Admin::UsersController < Admin::BaseController
  resources User,
    permit_fields: [:password, :password_confirmation],
    datatables: {
      search: [:username, :email],
      fields: [:username, :email]
    }
end

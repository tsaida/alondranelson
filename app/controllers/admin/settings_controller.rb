class Admin::SettingsController < Admin::BaseController
  def show
    render action: 'edit'
  end

  def update
    ActiveRecord::Base.transaction do
      (params[:settings] || {}).each_entry do |k, v|
        Settings[k] = v
      end
    end
    respond_to do |format|
      format.json do
        render json: Settings.all.to_json
      end
      format.js do
        render text: @n.settings.sorting
      end
      format.html do
        flash[:notice] = "Settings have been saved!"
        render action: "edit"
      end
    end
  end

  def create
    # inline editable
    if params[:name] && params[:pk]
      if params[:node_id]
        @n = Node.find(params[:node_id])
        @n.settings[params[:name]] = params[:value]
      else
        Settings[params[:name]] = params[:value]
      end
    end
    if params[:node] && params[:node][:rails_settings_scoped_settings] && params[:node][:rails_settings_scoped_settings][:sorting]
      if @n = Node.find(params[:node_id])
        @n.settings.sorting = params[:node][:rails_settings_scoped_settings][:sorting]
      end
    end
    update
  end
end

class Admin::NodesController < Admin::BaseController
  resources Node,
    belongs_to: -> (c) { c.load_parents },
    datatables: {
      fields: [:image, :node_date, :name, :title, :path, :slug, :un, :kind, :image_alt, :link_url, :link_text, :width, :height, :image_max, :reference_node_id, :link_new_window, :description, :template, :email, :attachment],
      row: -> (row, res) {
        res[:reference_node] = row.reference_node.attributes if row.reference_node
        res[:date] = row.node_date.try(:strftime, '%B %d, %Y')
        res[:tags] = row.related_tags.map(&:name).join(", ") if row.kind == 'video'
        res[:attachment_url] = row.attachment? ? row.attachment.url : '';
      },
      search: [:name, :title, :slug, :description, :link_text, :node_date],
      filter: :collection
    }

  def load_parents
    @nodes_hierarchy = []
    if params[:nodes_path]
      @node = node = Node.find_by_path(params[:nodes_path])
      raise ActiveRecord::RecordNotFound unless node
      @nodes_hierarchy << @node
      @page_node = node if node.page
      while node.node
        node = node.node
        @page_node = node if @page_node.nil? && node.page
        @nodes_hierarchy << node
      end
      @node_info = Site.get(@node)
      if @node_info
        @max_active = @node_info[:max_active]
        @max_count = @node_info[:max_count]
      end
    end
    @node
  end

  def collection(m)
    m = m.where(kind: params[:kind]) if params[:kind].present?
    m = m.where(un: params[:un]) if params[:un].present?
    m
  end

  def before_save
    if params[:node] && params[:node][:image] && params[:node][:image] == ''
      @node.image = nil
    end
    if params[:node] && params[:node][:attachment] && params[:node][:attachment] == ''
      @node.attachment = nil
    end
    if @node.kind == 'module'
      @template_was = @node.template
    end
  end

  def after_save
    if @node.valid?
      if @node.node && @node.node.kind == "genericpages" && @node.node.node.nil?
        @node.build_reference_node(kind: "alias", reference_node: @node) unless @node.reference_node
        @node.reference_node.name = @node.name || @node.title
        @node.reference_node.save
        @node.save
      end
      
      if params[:node] && params[:node][:inverse_relation_ids]
        params[:node][:inverse_relation_ids].reject(&:blank?).each_with_index do |id, index|
          if nr = @node.inverse_node_relations.find_by_node_id(id)
            nr.update_attributes(weight: index + 1)
          end
        end
      end
      
      if @node.kind == 'module' && @node.image?
        if @template_was && @template_was != @node.template
          @node.image.reprocess!
        end
      end
      
      params[:related_nodes] = [] unless params[:related_nodes]
      
      # add / remove tags
      if params[:related_nodes] && params[:related_nodes].is_a?(Array)
        new_relations = params[:related_nodes].collect{|c| c.to_i}
        old_relations = @node.related_ids

        to_remove = old_relations - new_relations
        to_add = new_relations - old_relations

        to_remove.each do |c|
          @node.node_relations.where(relation_id: c).delete_all
          @node.inverse_node_relations.where(node_id: c).delete_all
        end
        to_add.each do |c|
          @node.node_relations.create(relation_id: c)
        end
        
        # set weights
        Node.transaction do
          @node.node_relations.each do |direct_relation|
            weight = params[:related_nodes].index(direct_relation.relation_id.to_s).to_i + 1
            direct_relation.update_attributes(weight: weight) unless direct_relation.weight == weight
          end
          @node.inverse_node_relations.each do |inverse_relation|
            inverse_weight = params[:related_nodes].index(inverse_relation.node_id.to_s).to_i + 1
            inverse_relation.update_attributes(inverse_weight: inverse_weight) unless inverse_relation.inverse_weight == inverse_weight
          end
        end
      end
            
      Site.node_created(@node) if params[:action] == "create"

      flash[:notice] = "Successfully saved!"
      if @node.node && !@node.page && !@node.node.page && !(@node.node.node.blank? && @node.node.kind == "global")
        redirect_to admin_node_path(@node.node)
      else
        redirect_to admin_node_path(@node)
      end
    else
      render action: :show
    end
  end
  
  def before_destroy
    if @node.kind == 'business' && featured = Node.find_by_un('featured_businesses').nodes.find_by(kind: :featured_business, reference_node_id: @node.id)
      featured.destroy
    elsif @node.kind == 'featured_business' && @node.reference_node
      @node.reference_node.update_attributes(link_new_window: false)
    end
  end
end

class Admin::IconsController < Admin::BaseController
  resources Icon.ordered,
    datatables: {
      search: [:name, :filename],
      collection: -> (c) { Icon.ordered },
      fields: [:icon, :name, :filename, :width, :height, :required],
      row: -> (r) {{exists: r.icon.present? }}
    }
end

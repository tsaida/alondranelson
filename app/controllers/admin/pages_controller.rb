class Admin::PagesController < Admin::BaseController
  resource Page,
    belongs_to: -> (c) { c.load_parents },
    only: [:show, :update]

  def load_parents
    @nodes_hierarchy = []
    @node = node = Node.find_by_path(params[:nodes_path])
    raise ActiveRecord::RecordNotFound unless node && node.page
    @nodes_hierarchy << @node
    while node.node
      node = node.node
      @nodes_hierarchy << node
    end
    @page = @node.page
    @page_node = @node
    @default_page = Node.find_by_node_id_and_un(nil, :default).page
    @node_info = Site.get(@node)
    @node
  end
  
  def before_update
    if params[:page][:node]
      @node.update_attributes(params[:page].require(:node).permit!)
      params[:page].delete(:node)
    end
  end
end

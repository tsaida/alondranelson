require 'csv'

class Admin::BaseController < ApplicationController
  include AdminHub::DeleteAttachment
  layout 'admin'

  before_filter :authenticate_user!

  after_filter do
    # clear cache
    Rails.cache.clear if request.method != "GET" || request.xhr?
  end
  
  def members_list
    @members = Node.find_by_un(:members).children.group(:email)
    
    response.headers['Content-Type'] = 'text/csv'
    response.headers['Content-Disposition'] = "attachment; filename=\"members.csv\""
  end
end

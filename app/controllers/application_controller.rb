class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery # with: :exception
  
  rescue_from ActiveRecord::RecordNotFound, with: :render_missing
  rescue_from ActionView::MissingTemplate,  with: :render_missing

  before_filter do
    if browser.ie6? or browser.ie7?
      @node = Node.find_by_kind_and_un(:exception, :unsupported_browser)
      render "exception/error", status: 200, layout: "browser_error"
    end
  end

  def render_missing
    @status_code = 404
    if request.path =~ /^\/admin\// && user_signed_in?
      render "exception/admin", status: 404, layout: "admin", format: :html
    else
      @node = Node.find_by_kind_and_un(:exception, :page_not_found)
      render "exception/error", status: 404, layout: "application", format: :html
    end
  end

  def action_missing(action, *args)
    render_missing unless method_for_action(action)
  end
  
  def action_cache_path
    params.except(:_).merge(format: request.format, request_method: request.method, mobile: "#{browser.tablet?}_#{browser.mobile?}")
  end

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || "/admin"
  end
end

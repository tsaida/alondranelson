class PagesController < ApplicationController
  caches_action :show, :book, :video_tag, cache_path: :action_cache_path.to_proc
  
  before_filter :load_parents, only: [:show, :index]
  
  def load_parents
    if params[:nodes_path].blank?
      @node = Node.find_by_node_id_and_un(nil, Site.homepage)
    else  
      params[:nodes_path] = 'events' if ['events/upcoming', 'events/past'].include?(params[:nodes_path])
      @node = Node.find_by_path(params[:nodes_path])
      if @node && @node.node_id.nil? && @node.un == Site.homepage.to_s
        redirect_to "/"
        return
      end
    end
    @node = Node.find_by(kind: :generic_page, slug: params[:nodes_path]) unless @node
    raise ActiveRecord::RecordNotFound unless @node && @node.page && @node.active
    @page = @node.page
    
    @nodes = Node.joins(:page).where(node_id: nil, active: true)
        
    @node
  end
  
  def show
    if @node.kind == 'generic_page'
      render 'generic'
    elsif @node.un?
      render @node.un
    else
      render "exception/error"
    end
  end
  
  def book
    @main_node = Node.find_by_un!(:books_page)
    @node = @main_node.children(:books).find(params[:id])
  end
  
  def video_tag
    @node = Node.find_by_path("videos")
    @tag = Node.find(params[:id])
    render 'videos_page'
  end
end

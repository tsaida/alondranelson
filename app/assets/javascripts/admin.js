//= require jquery
//= require jquery_ujs

$.esc = function(html) {
  if (!html)
    return "";
  return html.replace(/&/g, '&amp;')
              .replace(/>/g, '&gt;')
              .replace(/</g, '&lt;')
              .replace(/"/g, '&quot;');
}

function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".fields").hide();
}

// modified for this site only
function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(link).parent().prev().append(content.replace(regexp, new_id));
  $('.fields input[type="checkbox"]').uniform();
}
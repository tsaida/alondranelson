
$(function() {
  
  // Show-hide nav
  $('.show_nav').off('click').on('click', function(e) {
    e.preventDefault();
    $('.h_nav_wrapper').fadeToggle(300);
    $('.h_nav').transit({ x: $('.h_nav').width() }, 0).transit({ x: 0 }, 300);
    if ($('.h_nav_wrapper').is(':visible')) {
      $('body').css('overflow-y', 'hidden');
    } else {
      $('body').css('overflow-y', 'visible');
    }
    return false;
  });
  
  $('.h_nav .close').off('click').on('click', function(e) {
    $('.h_nav').transit({ x: $('.h_nav').width() }, 300);
    $('.h_nav_wrapper').delay(300).fadeOut(300);
    $('body').css('overflow-y', 'visible');
    return false;
  });
    
  $('.praises_wrapper').find('.main_links.type4').children('a').on('click', function(e){
    e.preventDefault();
    $('.praises_wrapper').find('.praise').slideDown(300);
    $(this).fadeOut(300);
    return false;
  });
  
  $(window).resize(function(){
    if ($('html').width() > 767) {
      $('.h_nav_wrapper, .h_nav').removeAttr('style');
    }
  }).trigger('resize');
});

function getLatestPost(container, url) {
  console.log('Welcome!  Fetching your information.... ');
  FB.api('/me', function(response) {
    console.log('Successful login for: ' + response.name);
  });
  FB.api('/' + url + '/feed?limit=1&access_token=492591327588665|DjIjEmJV5yKZHh5t373gmQiOgV8', function(response) {
    if (response.error) {
      console.log("Facebook Post Error: " + response.error.message);
    } else {
      post = response.data[0];
      $(container).html(post.message);
    }
  });
}

var adjustViewport = function() {
  var iOSMobile = navigator.userAgent.match(/(iPhone|iPod)/g);
  if (!iOSMobile)
    return;
    
  var content_width, screen_dimension;

  if (window.orientation == 0 || window.orientation == 180) {
    // portrait
    content_width = 400;
    screen_dimension = screen.width * 0.98; // fudge factor was necessary in my case
  } else if (window.orientation == 90 || window.orientation == -90) {
    // landscape
    content_width = 767;
    screen_dimension = screen.height;
  }
  
  var viewport_scale = screen_dimension / content_width;
  
  // resize viewport
  $('meta[name=viewport]').attr('content', 'width=' + content_width + ', minimum-scale=' + viewport_scale + ', maximum-scale=' + viewport_scale);
  
  // If you want to hide the address bar on orientation change, uncomment the next line
  window.scrollTo(0, 0);
}

adjustViewport();
window.document.addEventListener('orientationchange', adjustViewport);

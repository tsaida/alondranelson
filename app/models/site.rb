
class Site < AdminHub::SiteStructure
  site_name "Alondra Nelson"
  homepage "homepage"

  build_tree do
    each(:page, is_page: true)
    each(:generic_page, is_page: true)
    
    has_one(:page, :homepage, "Home") do
      has_many(:modules, "Modules")
    end
    
    has_one(:page, :about_page, "About") do
      has_many(:modules, "Modules")
    end
    
    has_one(:page, :books_page, "Books") do
      has_many(:books, "Books")
    end
    
    each(:book, is_page: true) do
      has_many(:buy_links, "Buy Links")
      has_many(:social_links, "Social Links")
      has_many(:praise_items, "Praise")
      has_many(:news_items, "In the News")
      has_many(:modules, "Modules")
    end
    
    has_one(:page, :events_page, "Events") do
      has_many(:events, "Events")
    end
    
    has_one(:link, :blog_link, "Blog")
    
    has_one(:page, :videos_page, "Videos") do
      has_many(:videos, "Videos") do
        has_many(:video_tags, "Tags")
      end
      has_many(:tags, "Tags")
    end
    
    has_one(:page, :contact_page, "Contact") do
      has_many(:social_media_items, "Social Media")
      has_many(:links, "Links")
    end
        
    has_many(:generic_pages, "Generic Pages")
    
    has_one(:global) do
      has_many(:all_modules, "Modules") do
        has_many(:book_images, "Book Images")
        has_many(:images, "Images")
      end
      has_one(:footer, :footer, "Footer")
      has_one(:google_codes, "Google Codes")
      has_one(:exception, :page_not_found, "Page Not Found", title: "Page Not Found", description: "The page you are looking for cannot be located. Please <a href='/'>click here</a> to return home.")
      has_one(:exception, :unsupported_browser, "Unsupported Browser", title: "Your browser is not supported", description: "Sorry! We do not support Internet Explorer 6 or 7.")
    end

    has_one(:page, :default, "Default", active: false, page: {sm_frequency: "daily", sm_priority: 0.5})
  end

  def self.admin_menu
    item(:home, "Homepage") do
      item("Edit", node: :homepage)
      item("Modules", node: [:homepage, :modules])
      item("Settings",  url: "/admin/home/page")
    end
    item(:info_circle, "About") do
      item("Edit", node: :about_page)
      item("Modules", node: [:about_page, :modules])
      item("Settings", url: "/admin/about/page")
    end
    item(:book, "Books") do
      item("Edit", node: :books_page)
      item("Books", node: [:books_page, :books])
      item("Settings", url: "/admin/books/page")
    end
    item(:calendar, "Events") do
      item("Edit", node: :events_page)
      item("Events", node: [:events_page, :events])
      item("Settings", url: "/admin/events/page")
    end
    item(:external_link, "Blog", node: :blog_link)
    item(:video_camera, "Videos") do
      item("Edit", node: :videos_page)
      item("Videos", node: [:videos_page, :videos])
      item("Tags", node: [:videos_page, :tags])
      item("Settings", url: "/admin/videos/page")
    end
    item(:envelope_o, "Contact") do
      item("Edit", node: :contact_page)
      item("Social Media", node: [:contact_page, :social_media_items])
      item("Links", node: [:contact_page, :links])
      item("Settings", url: "/admin/contact/page")
    end
    item(:files_o,  "Generic Pages", node: :generic_pages)
    item(:globe, "Global") do
      item(:th_large,       "All Modules",  node: [:global, :all_modules])
      item(:info_circle,    "Footer", url: "/admin/global/footer")
      item(:file_picture_o, "Icons", url: "/admin/icons")
      item(:cogs,           "Default Page Settings", url: "/admin/default/page")
      item(:warning,        "Page Not Found", url: "/admin/global/page-not-found")
      item(:times_circle_o, "Unsupported Browser", url: "/admin/global/unsupported-browser")
    end
    item(:gear, "Settings") do
      item(:code, "Google Codes", url: "/admin/settings/google")
      item(:users, "Users",       url: "/admin/users")
    end
  end
end

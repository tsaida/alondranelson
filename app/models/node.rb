class Node < ActiveRecord::Base
  include RailsSettings::Extend
  extend FriendlyId
  friendly_id :slug_candidates, use: [:scoped, :finders], scope: :node_id

  belongs_to :nodeable, polymorphic: true
  belongs_to :node
  belongs_to :reference_node, class_name: "Node"
  has_many :nodes, dependent: :destroy
  has_many :reference_nodes, class_name: "Node", foreign_key: :reference_node_id
  has_one :page, dependent: :destroy
  
  has_many :node_relations, dependent: :destroy
  has_many :relations, -> { order("nodes.title") }, through: :node_relations
  
  has_many :inverse_node_relations, -> { order("node_relations.weight") }, class_name: "NodeRelation", foreign_key: :relation_id, dependent: :destroy
  has_many :inverse_relations, -> { order("node_relations.weight") }, through: :inverse_node_relations, source: :node

  accepts_nested_attributes_for :nodeable
  accepts_nested_attributes_for :page
  accepts_nested_attributes_for :node
  accepts_nested_attributes_for :nodes, reject_if: -> (c) { c[:name].blank? }, allow_destroy: true

  # attachments
  has_image :image, styles: -> (c) { c.instance.get_image_styles }, default_style: "default", index_style: "thumb", source_file_options: { all: '-background transparent' }
  validates_presence_of :image, if: -> (c) { c.image_required }
  
  has_attachment :attachment, unless: -> (c) { c.attachment_type == "pdf" }
  has_pdf :attachment, if: -> (c) { c.attachment_type == "pdf" }
  validates_presence_of :attachment, if: -> (c) { c.attachment_required }
  validates_attachment_content_type :attachment, content_type: /^video\//, if: -> (c) { c.attachment_type == "video" }, message: "is not a vidio file"
  validates_attachment_content_type :attachment, content_type: /^audio\//, if: -> (c) { c.attachment_type == "audio" }, message: "is not an audio file"
  validates_attachment_content_type :attachment, content_type: /^image\//, if: -> (c) { c.attachment_type == "image" }, message: "is not an image file"
  validates_attachment_content_type :attachment, content_type: [
    "pdf/adobe", "binary/octet-stream", "application/octet-stream", "application/x-pdf", "application/x-octet-stream",
    "application/x-download", "application/download", "application/force-download", "application/pdf", "doesn/matter"
    ], if: -> (c) { c.attachment_type == "pdf" }, message: "is not a PDF file"

  scope :actives, -> { where(active: true) }

  before_save do
    if path.blank? || slug_changed?
      get_path
      update_paths
    end
    if name_changed? && name?
      name = name.to_s.strip
    end
    
    if ['social_link', 'buy_link'].include?(kind) && self.new_record?
      self.weight = Node.where(kind: kind).count + 1
    end
    
    if self.image? && (width_changed? || height_changed?)
      self.image = self.image
    end
  end
      
  def update_paths
    return unless id?
    nodes.each do |node|
      # node.node = self
      node.get_path
      node.save
      node.update_paths
    end
  end
  
  #
  #
  #
  def child(kind, un=nil)
    (un.nil? ? nodes.find_by_kind(kind) : nodes.find_by_kind_and_un(kind, un))
  end

  def children_references(kind, un=nil)
    child(kind, un).nodes.references(:reference_node).includes(:reference_node).order(:weight).where(active: true, "reference_nodes_nodes.active" => true).collect{|c| c.reference_node}
  end

  def children(kind=nil, un=nil)
    if kind.nil? && un.nil?
      nodes.where(active: true).order(:weight)
    else
      child(kind, un).nodes.where(active: true).order(:weight)
    end
  end
  
  # related nodes
  def related_ids
    (node_relations.pluck(:relation_id, :weight) + inverse_node_relations.pluck(:node_id, :inverse_weight)).sort_by{ |r| r[1] }.map{ |r| r[0] }.uniq
  end
  
  def related_videos
    ids = related_ids
    if ids.present?
      Node.find_by_un(:videos).children.where("id IN (#{ ids.join(',') })").sort_by{ |v| ids.index v.id }
    else
      []
    end
  end
  
  def related_tags(ids=nil)
    ids ||= related_ids
    if ids.present?
      Node.find_by_un(:tags).children.where("id IN (#{ ids.join(',') })").sort_by{ |p| ids.index p.id }
    else
      []
    end
  end
  
  def get_social(un)
    if kind == 'footer'
      case un
      when :fb
        link_text
      when :tw
        link_url
      when :in
        description
      when :yt
        email
      end
    end
  end

  #
  # Slug generation
  #
  def slug_candidates
    [:name_slug, :name_and_sequence_slug]
  end

  def name_slug
    if name != "default" || kind == "page"
      ((name.present? ? name : nil) || (title.present? ? title : nil) || (link_text.present? ? link_text : nil) || un || kind).to_s
    else
      kind.to_s
    end
  end

  def name_and_sequence_slug
    if name != "default" || kind == "page"
      slug = ((name.present? ? name : nil) || (title.present? ? title : nil) || (link_text.present? ? link_text : nil) || un || kind).to_s.parameterize
    else
      slug = kind.to_s.parameterize
    end
    sequence = (node_id.nil? ? Node.where(node_id: nil) : node.nodes).where("slug like '#{slug}-%'").count + 2
    "#{slug}-#{sequence}"
  end

  def should_generate_new_friendly_id?
    (name.present? && name_changed?) || (title.present? && title_changed?) || (link_text.present? && link_text_changed?) || slug.blank?
  end

  def get_path
    n = self
    p = [slug]
    while n.node
      n = n.node
      p << n.slug
    end
    self.path = p.reverse.join("/")
  end

  def to_param
    if path.blank?
      get_path
      save
    end
    path
  end

  #
  # Image styles
  #
  def get_image_styles
    def stylefor(width, height)
      if width == 0
        return if height == 0
        image_max ? "x#{height}>" : "x#{height}"
      elsif height == 0
        image_max ? "#{width}>" : "#{width}"
      else
        image_max ? "#{width}x#{height}>" : "#{width}x#{height}#"
      end
    end
    {
      thumb: stylefor(width / 2, height / 2),
      default: stylefor(width, height) || "",
      retina: stylefor(width * 2, height * 2)
    }.reject{|k, v| v.nil?}
  end
end

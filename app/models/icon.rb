class Icon < ActiveRecord::Base
  Paperclip.interpolates :icon_filename do |attachment, style|
    attachment.instance.filename
  end

  has_attached_file :icon, 
                styles: lambda {|i| t = Icon.find(i.instance.id); { medium: "#{t.width}x#{t.height}#" } },
                path: ":rails_root/public/system/icons/:icon_filename",
                url: "/:icon_filename",
                storage: :filesystem,
                default_style: :medium

  validates_attachment_content_type :icon, content_type: /^image\//
  
  after_save do
    if self.icon? && File.exists?(self.icon.path)
      FileUtils.ln_sf(self.icon.path, Rails.root.join('public', self.filename))
    end
  end
  
  scope :ordered, -> { order(:created_at) }
end

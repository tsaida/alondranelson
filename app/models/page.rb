class Page < ActiveRecord::Base
  belongs_to :node

  has_image :fb_image, styles: { default: "200x200#", retina: "400x400#" }
end

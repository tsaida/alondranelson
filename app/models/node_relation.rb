class NodeRelation < ActiveRecord::Base
  belongs_to :node
  belongs_to :relation, class_name: "Node"
end

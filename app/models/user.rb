class User < ActiveRecord::Base
  devise :database_authenticatable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]

  attr_accessor :current_password

  validates :username, presence: true, uniqueness: true
end

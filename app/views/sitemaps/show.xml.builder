
base_url = "http://#{request.host_with_port}"

default = Node.find_by_path("default").page

cache do
  xml.instruct!
  xml.urlset "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do
    Page.includes(:node).references(:node).where.not(node_id: nil).where("nodes.active" => true).order(:id).each do |page|
      node = page.node
      next if node.nodeable && node.nodeable.respond_to?(:active) && !node.nodeable.active
      xml.url do
        xml.loc base_url + path_for(node).to_s
        xml.lastmod node.updated_at.to_datetime
        xml.changefreq page.sm_frequency.blank? ? default.sm_frequency : page.sm_frequency
        xml.priority page.sm_priority.blank? ? default.sm_priority : page.sm_priority
      end
    end
  end
end

require 'stringio'

module Admin::BaseHelper
  def try_render(*actions)
    # /:action/:parent_parent_un/parent_un_item
    # /:action/:parent_parent_un/parent_kind_item
    # /:action/:parent_parent_kind/parent_kind_item
    # /:action/:parent_un_item
    # /:action/:parent_kind_item
    # /:action/:parent_un/:un
    # /:action/:kind/:un
    # /:action/:parent_kind/:kind
    # /:action/:un
    # /:action/:kind
    # /:action/:parent_un/item (when @node.kind is nil)
    # /:action/:parent_kind/item (when @node.kind is nil)
    actions.each do |action|
      if @node.node && @node.node && @node.node.node && @node.node.node.un.present? && lookup_context.exists?("#{ @node.node.un }_item", ["admin/nodes/#{action}/#{@node.node.node.un}"], true)
        return render("admin/nodes/#{action}/#{@node.node.node.un}/#{@node.node.un}_item")
      end
      if @node.node && @node.node && @node.node.node && @node.node.node.un.present? && lookup_context.exists?("#{ @node.node.kind }_item", ["admin/nodes/#{action}/#{@node.node.node.un}"], true)
        return render("admin/nodes/#{action}/#{@node.node.node.un}/#{@node.node.kind}_item")
      end
      if @node.node && @node.node && @node.node.node && @node.node.node.kind.present? && lookup_context.exists?("#{ @node.node.kind }_item", ["admin/nodes/#{action}/#{@node.node.node.kind}"], true)
        return render("admin/nodes/#{action}/#{@node.node.node.kind}/#{@node.node.kind}_item")
      end
      if @node.node && @node.node.un.present? && lookup_context.exists?(@node.node.un + "_item", ["admin/nodes/#{action}"], true)
        return render("admin/nodes/#{action}/#{@node.node.un}_item")
      end
      if @node.node && @node.node.kind.present? && lookup_context.exists?(@node.node.kind + "_item", ["admin/nodes/#{action}"], true)
        return render("admin/nodes/#{action}/#{@node.node.kind}_item")
      end
      if @node.un.present? && @node.node && lookup_context.exists?(@node.un, ["admin/nodes/#{action}/#{@node.node.un}"], true)
        return render("admin/nodes/#{action}/#{@node.node.un}/#{@node.un}")
      end
      if @node.un.present? && @node.kind.present? && lookup_context.exists?(@node.un, ["admin/nodes/#{action}/#{@node.kind}"], true)
        return render("admin/nodes/#{action}/#{@node.kind}/#{@node.un}")
      end
      if @node.kind.present? && @node.node && lookup_context.exists?(@node.kind, ["admin/nodes/#{action}/#{@node.node.kind}"], true)
        return render("admin/nodes/#{action}/#{@node.node.kind}/#{@node.kind}")
      end
      if @node.un.present? && lookup_context.exists?(@node.un, ["admin/nodes/#{action}"], true)
        return render("admin/nodes/#{action}/#{@node.un}")
      end
      if @node.kind.present? && lookup_context.exists?(@node.kind, ["admin/nodes/#{action}"], true)
        return render("admin/nodes/#{action}/#{@node.kind}")
      end
      if @node.node && @node.node.un.present? && @node.kind.nil? && lookup_context.exists?("item", ["admin/nodes/#{action}/#{@node.node.un}"], true)
        return render("admin/nodes/#{action}/#{@node.node.un}/item")
      end
      if @node.node && @node.node.kind.present? && @node.kind.nil? && lookup_context.exists?("item", ["admin/nodes/#{action}/#{@node.node.kind}"], true)
        return render("admin/nodes/#{action}/#{@node.node.kind}/item")
      end
    end
    actions.each do |action|
      if lookup_context.exists?("default", ["admin/nodes/#{action}"], true)
        return render("admin/nodes/#{action}/default")
      end
    end
    nil
  end

  def menu_builder
    #attributes = {}
    menu = Site.get_admin_menu
    #return menu.inspect
    result = StringIO.new

    bread_crumbs = []
    named_routes = {}

    found_url = ""
    found_item = nil

    find_active = lambda do |items|
      items.each do |item|
        item[:is_active] = false
        next if item[:separator]

        node = nil
        if item[:node]
          # node is array of Nodes un which represents hierarchy
          if item[:node].is_a?(Array) || item[:node].is_a?(Symbol)
            node_id = nil
            Array(item[:node]).each do |un|
              node = Node.find_by_node_id_and_un(node_id, un)
              node_id = node.id
            end
          elsif item[:node].is_a?(Node)
            node = item[:node]
          else
            node = Node.find_by_path(item[:node])
          end
          item[:_node] = node
          item[:_name] = node.name? ? node.name : node.title
        end

        if node && !item[:generated]
          children = node.nodes.includes(:page).references(:page).where("pages.id is not null").order(:weight).to_a
          if children.present?
            item[:node] = nil
            item[:children] = [{name: "Edit", icon: "edit", node: node, generated: true}] + children.collect{|c| {name: c.name || c.title, node: c.path, generated: true}}
          end
        end

        if item[:url] || item[:node]
          if item[:url] && Regexp.new("^" + item[:url] + "(/\\d+)?(\/(page|edit|new))?$").match(request.path)
            if item[:url].length > found_url.length
              found_url = item[:url]
              found_item = item
            end
          end
          if node && @nodes_hierarchy && (params[:action] == "index" ? item[:nodes] : !item[:nodes])
            if @nodes_hierarchy.collect{|c| c.path}.include?(node.path)
              found_url = "/admin/" + node.path
              found_item = item
            end
          end
        elsif item[:children]
          find_active.call(item[:children])
        end
      end
    end

    find_active.call(menu)
    found_item[:is_active] = true if found_item

    update = lambda do |items|
      active = false
      items.each do |item|
        next if item[:separator]

        is_active = item[:is_active]
        is_active||= update.call(item[:children]) if item[:children]
        active||= is_active
        klass = []
        klass << "first" if item == menu[0]
        klass << "active" if is_active || item[:open]
        #attributes[item.hash] = {klass: klass.join(" "), item: item[:name]}
        item[:_klass] = klass.join(" ")
        if is_active
          if item[:breadcrumb_append]
            bread_crumbs << item[:breadcrumb_append].call
          end
          bread_crumbs << item
          if item[:breadcrumb_prepend]
            bread_crumbs << item[:breadcrumb_prepend].call
          end
        end
      end
      active
    end
    update.call(menu)
    @bread_crumbs = bread_crumbs.reject{|c| !c.present?}.collect{|c| c.kind_of?(String) ? {name: c} : c}.reverse
    
    if @nodes_hierarchy.present?
      @bread_crumbs = []
      @nodes_hierarchy.reverse.each do |node|
        @bread_crumbs << {name: node.name || node.title || node.un || node.kind, url: params[:controller] != "admin/nodes" || node != @node ? admin_node_path(node) : nil }
      end
      if params[:controller] == "admin/pages"
        @bread_crumbs << {name: "Settings" }
      end
    end

    if @bread_crumbs.blank? && @title.blank?
      @title = "Admin"
    else
      @title = "Admin : " + (@title || @bread_crumbs.collect {|i| i[:name]}.join(" : "))
    end

    @page_title = @title
    submenu = lambda do |items|
      items.each do |item|
        #attrs = attributes[item.hash]
        children = (!item[:url] && !item[:node]) && item[:children]
        next if children && children.length == 0

        if item[:separator]
          result << "<li><hr /></li>"
          next
        end

        url = item[:url]
        if !url && item[:node]
          node = item[:_node] || item[:node]
          if item[:nodes]
            url = nodes_admin_node_path(node)
          else
            url = admin_node_path(node)
          end
        end
        result << "<li class='" << item[:_klass] << "'" << (item[:_node] && !item[:_node].active ? " style='opacity:0.5'" : "") << ">"
        result << "<a href='" << (children ? "javascript:;" : url) << "'>"
        result << " <i class='fa-" << item[:icon].to_s.gsub('_', '-') << "'></i> " if item[:icon]
        result << "<span class='title'>" << h(item[:name] || item[:_name]) << "</span>"
        result << "<span class='arrow#{item[:_klass].include?('active') ? ' open' : ''}'></span>" if children
        result << "</a>"
        if children
          result << "<ul class='sub-menu'" << (item[:open]) << ">"
          submenu.call(children)
          result << "</ul>"
        end
        result << "</li>"
      end
    end

    submenu.call(menu)
    result.string.html_safe
  end

  def breadcrumbs(homeurl = "/admin")
    result = StringIO.new
    result << "<li><i class='fa-home'></i> <a href='#{homeurl}'>Home</a> "
    result << "<i class='fa-angle-right'></i>" if @bread_crumbs.length > 0
    result << "</li>"

    @bread_crumbs.each do |item|
      result << "<li>"
      if item[:url]
        result << "<a href='" << item[:url] << "'>"
        result << h(item[:name] || item[:_name])
        result << "</a>"
      else
        result << h(item[:name] || item[:_name])
      end
      result << " <i class='fa-angle-right'></i> " if item != @bread_crumbs[-1]
      result << "</li>"
    end
    result.string.html_safe
  end
  
  def link_to_remove_fields(name, f, msg='Do you really want to delete this entry?')
    f.hidden_field(:_destroy) + link_to(name, '#remove', onclick: "var t=this; bootbox.confirm('#{msg}', function(r) {if(r) remove_fields(t)});return false;")
  end
  
  def link_to_add_fields(name, f, association, path='', locals={}, options={})
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.admin_fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(path + (options[:kind] || association).to_s.singularize + "_fields", locals.merge!(:f => builder))
    end
    link_to(name, "#", {onclick: "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;"}.merge(options))
  end
end

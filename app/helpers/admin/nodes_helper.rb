module Admin::NodesHelper
  def build_tree(node)
    res = []
    node.order(:weight).each do |n|
      has_page = n.page.present?
      res << "<li style='" + (n.active ? "" : "opacity:0.5") + "'>"
      res << "<b>" if has_page
      res << "<a href='" + admin_node_path(n) + "'>" + h(n.name) + " (" + h(n.kind) + (n.un.present? ? ": <small>" + n.un + "</small>" : "") + ")"
      res << "</a>"
      res << " <span style='color:red'>-&gt; <a href='" + admin_node_path(n.reference_node) + "'>" + h(n.reference_node.name) + " (" + h(n.reference_node.kind) + ")</span>" if n.reference_node
      res << "</b>" if has_page
      res << build_tree(n.nodes)
      res << "</li>"
    end
    res.length > 0 ? "<ol>" + res.join + "</ol>" : ""
  end
  
  def get_tag_selectables(all=[], tags=[])
    existing = all.select{ |row| tags.include?(row[1]) }.sort_by{ |row| tags.index row[1] }
    other = all - existing
    return (existing + other)
  end
end

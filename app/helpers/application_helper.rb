module ApplicationHelper
  def get_title
    if @title
      @title
    elsif @page && @page.browser_title.present?
      @page.browser_title
    elsif @node
      if @node.title?
        "Alondra Nelson | #{@node.title}"
      elsif @node.name?
        "Alondra Nelson | #{@node.name}"
      else
        "Alondra Nelson"
      end
    else
      "Alondra Nelson"
    end
  end
  
  def meta_viewport
    if browser.tablet?
      "width=767, user-scalable=no"
    elsif browser.mobile?
      "width=400, user-scalable=no"
    else
      "width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
    end
  end
  
  # current page
  def cp(path)
    'active' if request.path =~ path && !(@node && @node.un == 'page_not_found')
  end
  
  def body_class
    return unless @node
    if @node.un?
      @node.un == 'homepage' ? nil : @node.un.gsub(/_page/, '')
    elsif @node.kind == 'book'
      'book_feature'
    end
  end
  
  def path_for(item)
    case item.kind
    when 'page'
      item.un == 'homepage' ? '/' : '/' + item.path.to_s
    when 'book'
      "/books/#{item.slug || item.id}"
    when 'generic_page'
      item.path.to_s.gsub(/^generic-pages/, '')
    else
      "/" + item.path.to_s
    end
  end
  
  def external_link(link)
    return unless link.present?
    link.match(/^(http|\/)/) ? link : "http://#{ link }"
  end
end

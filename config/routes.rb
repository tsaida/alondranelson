Rails.application.routes.draw do
  root to: "pages#show"
    
  get "/books/:id",   to: "pages#book",       as: :book
  get "/videos/:id",  to: "pages#video_tag",  as: :video_tag
    
  resource :sitemap,  only: :show
  
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, path: "admin", path_names: {sign_in: 'login', sign_out: 'logout'}

  namespace :admin do
    post 'base/delete_attachment'
        
    resource :dashboard, :path => "/"
    resources :users, :settings, :icons
        
    resources :nodes, as: :root_nodes
    resource :node, path: "*nodes_path" do
      member do
        get :nodes, action: :index
      end
      resource :page, only: [:show, :update]
    end
  end
  
  match "*nodes_path", to: "pages#show", via: [:get, :post]
end

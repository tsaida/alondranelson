/*
<!-- BEGIN CORE PLUGINS -->
<script src="/admin/assets/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/admin/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/admin/assets/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="/admin/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="/admin/assets/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="/admin/assets-misc/jquery.dirtyforms.js" type="text/javascript"></script>
<script src="/admin/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/admin/assets/scripts/app.js" type="text/javascript"></script>
<script src="/admin/assets-misc/bootbox.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
*/

/*
 * reordering: false
 * onDelete: null
 */
function datatables(id, url) {
  var defaults = {
    sAjaxSource: url,
		bProcessing: true,
		bServerSide: true,
    iDisplayLength: 10
  };
  var dom = $(id);
  var path = url.replace(/\?.*$/, '');

  var buttons = {
    "toggle": function(row, active, disabled) {
      return "<a " + (disabled ? "disabled='disabled' " : "") + "class='btn btn-default btn-xs green-stripe toggle' href='#' title='Toggle'><span class='fa" + (active && active != 'f' ? '-eye' : "-eye-slash' style='color:#888'") + "'></span></a> ";
    },
    "edit": function(row, url, target, disabled) {
      var u = url.match(/\/nodes($|\?)/) && row.path ? ("/admin/" + row.path) : (url + "/" + (row.id || row.DT_RowId));
      return "<a " + (disabled ? "disabled='disabled' " : "") + "class='btn btn-default btn-xs green-stripe' href='" + u + "/edit' title='Edit' " + (target ? "target='" + target + "'" : '') + "><span class='fa-pencil'></span></a> ";
    },
    "delete": function(row, url, disabled) {
      var u = url.match(/\/nodes($|\?)/) && row.path ? ("/admin/" + row.path) : (url + "/" + (row.id || row.DT_RowId));
      return "<a " + (disabled ? "disabled='disabled' " : "") + "class='btn btn-default btn-xs green-stripe delete' href='" + u + "' title='Remove'><span class='fa-trash-o'></span></a> ";
    }
  };
  return {
    /*
     * Options:
     *  reordering: true|false
     *  onReorder: function
     *  onToggle: function
     *  onDelete: function
     *
     */
    init: function(options) {
  		var othis = this;
      if (options.reordering) {
        //options.include_settings = [];
        options.bPaginate = options.bPaginate || false;
        options.bSort = options.bSort || false;
        options.bLengthChange = options.bLengthChange || false;
        options.bFilter = options.bFilter || false;
        options.bInfo = options.bInfo || false;
        options.iDisplayLength = options.iDisplayLength || 1000;
        options.aoColumns = [this.columns.sort()].concat(options.aoColumns);
        defaults.sAjaxSource+= (defaults.sAjaxSource.indexOf("?") >= 0 ? "&" : "?") + "with_weight=true";
        for (var i=0; i<options.aoColumns.length; i++)
          options.aoColumns[i].bSortable = false;
      }
      if (options.include_settings)
        defaults.sAjaxSource+= (defaults.sAjaxSource.indexOf("?") >= 0 ? "&" : "?") + "include_settings=" + options.include_settings.join(",");

  		options.fnRowCallback = options.fnRowCallback || function(nRow, aData, iDisplayIndex) {
  		  if (options.rowCallback) options.rowCallback(nRow, aData, iDisplayIndex);
  		  if (aData.active != undefined)
  		    $('td', nRow).css({opacity: aData.active == false ? 0.5 : 1});

        // toggle
  		  $('a.toggle', nRow).on('click', function() {
          var d = $('span', $(this));
          var res = $.ajax(path + '?toggle=true&id=' + aData.DT_RowId, {async: false}).responseJSON;
          if (typeof(res) == "object" && res.error) {
            bootbox.alert("ERROR: " + res.error);
          } else if (typeof(res) != "boolean") {
            bootbox.alert("ERROR: Couldn't save, it has errors!");
          } else {
            d.removeClass().addClass("fa" + (res ? '-eye' : '-eye-slash'));
            d.css({color:res ? '' : '#888'});
          }
	        if (options.onToggle)
            options.onToggle();
	        else
            options.datatable.fnDraw();
          return false;
  		  });

  		  $('a.delete', nRow).on('click', function() {
  		    var url = $(this).attr('href');
  		    bootbox.confirm("Are you sure?", function(res) {
  		      if (res)
    		      $.ajax(url, {method: 'DELETE', async: false, success: function() {
    		        if (options.onDelete)
  		            options.onDelete();
    		        else
  		            options.datatable.fnDraw();
    		      }});
  		    });
  		    return false;
  		  });
  		};

  		options.fnServerData = options.fnServerData || function(sSource, aoData, fnCallback) {
  		  $.getJSON(sSource, aoData, function(json) {
  		    othis.data = json;
  		    fnCallback(json);
		    });
		  };

      dom.addClass("table table-striped table-bordered table-hover");
      this.datatable = dom.dataTable($.extend(defaults, options));
      options.datatable = this.datatable;
      $(id + '_wrapper .dataTables_filter input').addClass("form-control input-medium");
      $(id + '_wrapper .dataTables_length select').addClass("form-control input-xsmall").select2();
      if (options.reordering) {
        this.datatable.rowReordering({sURL: path + "?reorder=true", sRequestType: "GET", fnSuccess: options.onReorder || $.noop});
      }
      return this;
    },

    button: function(options) {
      return "<a class='btn btn-default btn-xs green-stripe " + (options["class"] || '') + "' href='" + (options.url || '#') + "' title='" + (options.title || '') + "'" + (options.target ? " target='" + options.target + "'" : "") + " style='min-width:24px'>" + 
        (options.text || '') + (options.icon ? " <span class='" + options.icon + "'></span>" : "") + "</a> ";
    },
    "buttons": buttons,
    "columns": {
      sort: function() {
        return {sWidth: "20", bSortable: false, sClass: "center reorder", mRender: function(data, type, row) {
          return "<span class='fa-sort' title='Reorder'></span>";
        }}
      },

      actions: function(options) {
        var os = $.extend({toggle: true, edit: true, trash: true, callback: function(row, button) {return true;}}, options || {});
        return {sWidth: "50", sClass: (options && options.sClass) || "align-right nowrap", bSortable: false, mRender: function(data, type, row) {
          var toggleRes = os.callback(row, 'toggle');
          var trashRes = os.callback(row, 'trash');
          var editRes = os.callback(row, 'edit');
          return (os.prepend ? os.prepend(data, type, row) : "") +
            ((os.toggle === 0 || os.toggle) && toggleRes != false ? buttons.toggle(row, row.active, toggleRes == 'disabled') : "") +
            (os.edit && editRes != false ? buttons.edit(row, path, editRes == 'disabled') : "") +
            (os.afteredit ? os.afteredit(data, type, row) : "") +
            (os.trash && trashRes != false ? buttons["delete"](row, path, trashRes == 'disabled') : "");
        }};
      }
    }
  };
}

(function () {
  'use strict';

  $.fn.datepicker.defaults.format = 'MM d, yyyy';
  $.fn.datepicker.defaults.rtl = false;
  $.fn.datepicker.defaults.autoclose = true;

  $('.date-picker').each(function() {
    if ($(this).hasClass('only_month')) {
      $(this).datepicker({ format: 'MM yyyy', viewMode: 'months', minViewMode: 'months' });
    } else {
      $(this).datepicker();
    }
  });

  $('.datetime-picker').each(function() {
    $(this).datetimepicker({ 
      weekStart: 1,
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      showMeridian: 1 
    });
  });
  
  $('.timepicker-default').timepicker();

  $('input[type=file]').on('change', function(e, p) {
    if (p == 'clear' && $(this).attr("data-exists")) {
      $(this).removeAttr("data-exists");
      $.post("/admin/base/delete_attachment?target=" + $(this).attr('data-target'));
    }
  });

  $('input[maxlength], textarea[maxlength]').maxlength({
    limitReachedClass: "label label-danger",
    alwaysShow: true
  });

  jQuery.fn.disableSelection = function() {
  	return this.each(function() {
  		$(this).css({
  			'MozUserSelect':'none',
  			'webkitUserSelect':'none'
  		}).attr('unselectable','on').bind('selectstart', function() {
  			return false;
  		});
  	});
  };

  jQuery.fn.enableSelection = function() {
  	return this.each(function() {
  		$(this).css({
  			'MozUserSelect':'',
  			'webkitUserSelect':''
  		}).attr('unselectable','off').unbind('selectstart');
  	});
  };

  jQuery.esc = function(html) {
    if (!html)
      return "";
    return html.replace(/&/g, '&amp;')
                .replace(/>/g, '&gt;')
                .replace(/</g, '&lt;')
                .replace(/"/g, '&quot;');
  }

	$('form').submit(function(event) {
    for (var key in CKEDITOR.instances)
      CKEDITOR.instances[key].updateElement();
		return true;
	});

  $('.colorpicker-default').colorpicker({
    format: 'hex'
  });

	// Dirty Forms
	$.DirtyForms.dialog = {
		//selector: '#unsavedChanges',
		fire: function(message, title) {
			bootbox.dialog({
				title: title,
				message: message,
				onEscape: function() {
					$.DirtyForms.choiceContinue = false;
					$.DirtyForms.choiceCommit();
				},
				buttons: {
					discard: {
						label: "Yes, discard changes",
						className: "btn-danger",
						callback: function() {
							$('form.form-horizontal, form.form-vertical').dirtyForms('setClean');
							$.DirtyForms.choiceContinue = true;
							$.DirtyForms.choiceCommit();
						}
					},
					cancel: {
						label: "Cancel",
						className: "btn-primary",
						callback: function() {
							$.DirtyForms.choiceContinue = false;
							$.DirtyForms.choiceCommit();
						}
					}
				}
			});
		},
		bind: function() {
		},
    refire: function(content) {
      return false;
    },
    stash: function() {
      return false;
    }
	};
	$('form.form-horizontal, form.form-vertical').dirtyForms({});

  jQuery.validator.setDefaults({
    errorElement: 'span',
    errorClass: 'help-block',
    focusInvalid: true,
    ignore: "",

    invalidHandler: function (event, validator) {
    },

    highlight: function (element) {
      $(element).closest('.form-group').addClass('has-error');
      $('.help-block', $(element).parent()).removeClass('validate-success');
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element).closest('.form-group').removeClass('has-error');
    },

    success: function (label) {
      label.closest('.form-group').removeClass('has-error');
      label.addClass('validate-success');
    }
  });
    
  $('.select2').each(function(){
    $(this).select2({
      search: !$(this).hasClass('no-search'),
      minimumResultsForSearch: $(this).hasClass('no-search') ? -1 : null
    });
  });
  
  $('.multi-select').multiSelect();

  // session timeout
  if (window.location.pathname != "/admin/login") {
    var $countdown;
    $('body').append('<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static"><div class="modal-dialog modal-small"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Your session is about to expire.</h4></div><div class="modal-body"><p><i class="fa fa-warning"></i> You session will be locked in <span id="idle-timeout-counter"></span> seconds.</p><p>Do you want to continue your session?</p></div><div class="modal-footer"><button id="idle-timeout-dialog-logout" type="button" class="btn btn-default">No, Logout</button><button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Keep Working</button></div></div></div></div>');
    $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
      idleAfter: 60 * 30, // idle timeout 5 minutes
      timeout: 30000,
      pollingInterval: 50,
      keepAliveURL: '/admin',
      serverResponseEquals: 'OK',
      onTimeout: function() {
        // logout
        $.ajax({url: "/admin/logout", type: "DELETE", success: function() {
          window.location = window.location.toString();
        }});
      },
      onIdle: function() {
        $('#idle-timeout-dialog').modal('show');
        $countdown = $('#idle-timeout-counter');
        $('#idle-timeout-dialog-keepalive').on('click', function () { 
            $('#idle-timeout-dialog').modal('hide');
        });
        $('#idle-timeout-dialog-logout').on('click', function () { 
            $('#idle-timeout-dialog').modal('hide');
            $.idleTimeout.options.onTimeout.call(this);
        });
      },
      onCountdown: function(counter) {
        $countdown.html(counter); // update the counter
      }
    });
  }

})();

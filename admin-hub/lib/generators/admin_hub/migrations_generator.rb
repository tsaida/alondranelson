require 'rails/generators/active_record'
require 'rails/generators/named_base'

module AdminHub
  class MigrationsGenerator < ActiveRecord::Generators::Base
    #argument :attributes, :type => :array, :default => [], :banner => "field:type field:type"
    source_root File.expand_path('../../templates', __FILE__)

    def copy_admin_hub_migration
      migration_template "migration_devise_username.rb", "db/migrate/add_username_to_users.rb"
      migration_template "migration_admin_hub.rb", "db/migrate/create_admin_hub.rb"
    end
  end
end

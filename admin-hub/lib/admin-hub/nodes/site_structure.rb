
class AdminHub::SiteStructure
  @@tree = nil
  @@current = nil
  @@nodes = nil
  @@seeding = false

  @@settings = {}

  #
  # Public Methods
  #
  def self.method_missing(name, *args, **params)
    if args.length > 0
      @@settings[name] = args[0]
    end
    @@settings[name]
  end

  def self.get(node)
    top = node
    nodes = [node]
    while top.node
      top = top.node
      nodes << top
    end

    tree = @@tree.dup
    res = nil
    nodes.reverse.each do |n|
      res = tree.find{|t| t[:un] == n.un.to_s && t[:kind] == n.kind.to_s}
      self.foreach_node(n) do |p|
        res = {} unless res
        children = (res[:children] || []) + (p[:children] || [])
        res = p.merge(res)
        res[:children] = children
      end
      break if res.blank? || (res[:collection] && n  == node.node)
      tree = res[:children]
    end
    res||= {children: []}
    self.foreach_node(node) do |p|
      children = (res[:children] || []) + (p[:children] || [])
      res = p.dup.merge(res)
      res[:children] = children
    end
    res
  end

  def self.create!
    @@seeding = true
    self.create_sub(@@tree, Node)
    @@seeding = false
  end

  def self.node_created(node)
    puts "Creating #{node.name}..." if @@seeding
    data = self.get(node)
    return if data.nil?
    node.build_page.save(browser_title: node.name) if data[:is_page] && !node.page
    data[:children].each_with_index do |item, weight|
      #next if item[:collection]
      sub = node.nodes.find_by_kind_and_un_and_name(item[:kind], item[:un], item[:name])
      unless sub
        attrs = {weight: weight + 1}
        Node.attribute_names.each do |name|
          attrs[name] = item[name.to_sym] if item.has_key?(name.to_sym)
        end
        sub = node.nodes.create(attrs)
        if item[:page] && item[:page].is_a?(Hash)
          sub.build_page(item[:page]).save
        end
        self.node_created(sub) unless item[:collection]
      end
    end
  end

  #
  # Private methods
  #
  def self.create_sub(tree, parent)
    tree.each_with_index do |item, weight|
      if item[:name].blank?
        node = parent.find_by_kind_and_un(item[:kind], item[:un])
      else
        #node = parent.find_by_kind_and_un_and_name(item[:kind], item[:un], item[:name])
        node = parent.find_by_kind_and_un(item[:kind], item[:un])
      end
      if node
        attrs = {}
        Node.attribute_names.each do |name|
          attrs[name] = item[name.to_sym] if item.has_key?(name.to_sym) && !item[name.to_sym].blank? && node[name.to_sym].blank?
        end
        node.update_attributes(attrs) unless attrs.empty?
      else
        attrs = {weight: weight + 1}
        Node.attribute_names.each do |name|
          attrs[name] = item[name.to_sym] if item.has_key?(name.to_sym)
        end
        node = parent.create(attrs)
        if item[:page] && item[:page].is_a?(Hash)
          node.build_page(item[:page]).save
        end
        self.node_created(node)
      end
      create_sub(item[:children], node.nodes) unless item[:collection]
    end
  end
  
  def self.foreach_node(node, &proc)
    kind = node.kind.to_s
    un = node.un.to_s
    @@nodes.each do |n|
      next if n[:kind] != kind || (!n[:un].blank? && n[:un] != un)
      if n[:under]
        next if !node.node
        next if n[:under].is_a?(Hash) && (n[:under][:kind].to_s != node.node.kind || n[:under][:un].to_s != node.node.un)
        next if !n[:under].is_a?(Hash) && n[:under].to_s != node.node.kind
      end
      proc.call(n)
    end
  end

  def self.each(kind, *args, **params, &proc)
    un = args[0].is_a?(Symbol) ? args.shift.to_s : nil
    children = []
    @@nodes << {kind: kind.to_s, un: un, children: children}.merge(params)
    if proc
      t = @@current
      @@current = children
      proc.call
      @@current = t
    end
  end

  def self.has_one(kind, *args, **params, &proc)
    un = args[0].is_a?(Symbol) ? args.shift : kind
    name = args.shift || un.to_s.humanize
    children = []
    @@current << {kind: kind.to_s, name: name.to_s, un: un.to_s, children: children}.merge(params)
    if proc
      t = @@current
      @@current = children
      proc.call
      @@current = t
    end
  end

  def self.has_many(kind, *args, **params, &proc)
    un = args[0].is_a?(Symbol) ? args.shift : kind
    name = args.shift || un.to_s.humanize
    children = []
    @@current << {collection: true, kind: kind.to_s, name: name.to_s, un: un.to_s, children: children}.merge(params)
    if proc
      t = @@current
      @@current = children
      proc.call
      @@current = t
    end
  end

  def self.build_tree(&proc)
    #if @@tree.nil? || Rails.env.development?
      @@tree = []
      @@nodes = []
      @@current = @@tree
      proc.call
    #end
  end

  @@menu = nil
  @@current_menu = nil

  def self.get_admin_menu
    #if @@menu.nil? || Rails.env.development?
      @@menu = []
      @@current_menu = @@menu
      self.admin_menu
    #end
    @@menu
  end

  def self.clear_admin_menu
    @@menu = nil
  end

  def self.item(*args, **params, &proc)
    if args.length > 0 && args[0].is_a?(Symbol)
      icon = args.shift
    end
    #icon = args.length > 1 ? args.shift : nil
    name = args[0]
    children = []
    @@current_menu << {icon: icon, name: name, children: proc ? children : nil}.merge(params)
    if proc
      t = @@current_menu
      @@current_menu = children
      proc.call
      @@current_menu = t
    end
  end
  
  def self.separate
    @@current_menu << {separator: true}
  end

  def self.item_subnodes(node, kind=nil, un=nil)
    node = Node.find_by_path(node) unless node.is_a?(Node)
    if node
      nodes = node.nodes.where(active: true).order(:weight)
      nodes = nodes.where(kind: kind) if kind.present?
      nodes = nodes.where(un: un) if un.present?
      nodes.each do |n|
        @@current_menu << {icon: nil, name: n.name, node: n}
      end
    end
  end
end


module AdminHub::Resources
  module ActionController
    #
    # options:
    #  key - model column to search by (default: :id)
    #  only - (default: [:index, :new, :create, :show, :edit, :update, :destroy])
    #  except - (except actions)
    #  notice - (default: {{model}} was successfully saved!)
    #  permit - permitted fields
    #  new_at - :top or :bottom, if it has weight, it will put new records at top or at bottom of list (default is :top)
    #  weight: false - no weight
    #  datatables:
    #    filter: same as collection, but runs earlier
    #    max_active: number - maximum number of active objects
    #    collection: :method
    #    collection: -> (model) {... return model}
    #    collection: -> (c, model) {...}
    #    search: [:title] - search in fields
    #    fields: [:id, :title, lambda {|r| r.title}]
    #    row: -> (row, model) {row << model.id}
    #    order: [:title]
    #
    # jquery.sortable:
    #   just name variable 'sortable_item', and make post request to create method
    #
    def resource(model, options={})
      options[:single] = true
      resources(model, options)
    end

    def resources(model, options={})
      instance = new
      single = options[:single]
      all_methods = single ? [:show, :edit, :update, :destroy] : [:index, :new, :create, :show, :edit, :update, :destroy]

      name = model.model_name.singular.to_s
      methods = Array(options[:only] || all_methods) - Array(options[:except])
      datatables = options[:datatables]
      notice_message = options[:notice] || (model.model_name.human + " was successfully saved!")
      permit = options[:permit]
      new_records_at = options[:new_at]
      belongs = Array(options[:belongs_to])

      methods.each do |method|
        methods-= [method] if instance_methods.include?(method) #or ([:index].include?(method) and !instance.method_for_action(method.to_s))
      end

      if options[:weight] == false
        has_weight = false
      else
        weight_field_name = options[:weight].present? ? options[:weight].to_s : "weight"
        weight_column = model.columns.find{|p| p.name == weight_field_name}
        has_weight = weight_column and weight_column.type == :integer
      end
      active_column = model.columns.find{|p| p.name == "active"}
      has_toggle = active_column and active_column.type == :boolean
      has_tags = model.respond_to?("taggable?") and model.taggable?
      max_active = (datatables ? datatables[:max_active] : -1) || -1

      define_method :_params do
        if params[name.to_sym]
          p = params.require(name.to_sym)
          unless permit
            res = p.permit!
            if p[:settings] and p[:settings].is_a?(Hash)
              res = res.dup
              res.delete(:settings)
            end
            if p[:rails_settings_scoped_settings] and p[:rails_settings_scoped_settings].is_a?(Hash)
              res = res.dup
              res.delete(:rails_settings_scoped_settings)
            end
            res.reject{|k| k.starts_with?("image.") || k.starts_with?("node.")}
          else
            p.permit(permit)
          end
        else
          {}
        end
      end

      # loaders
      if belongs.present?
        if belongs.length == 1 and belongs[0].is_a?(Proc)
          define_method :_get_model do
            if @_load_parents_result
              if @_load_parents_result.is_a?(ActiveRecord::Associations::CollectionProxy)
                @_load_parents_result
              elsif @_load_parents_result.is_a?(Class)
                model
              else
                @_load_parents_result.method(single ? model.model_name.singular : model.model_name.plural).call
              end
            else
              model
            end
          end

          define_method :_load_parents do
            @_load_parents_result = belongs[0].call(self)
          end
          before_filter :_load_parents
        else
          define_method :_load_parents do
            p = nil
            belongs.each do |parent|
              parent_name = parent.to_s.underscore.singularize
              parent = p.method(parent_name.pluralize).call if p
              if single && !params[parent_name + "_id"].present?
                pm = params["id"]
              else
                pm = params[parent_name + "_id"]
              end
              if parent.respond_to?(:friendly)
                p = parent.friendly.find(pm)
              else
                p = parent.by_key(pm).first
              end
              raise ActiveRecord::RecordNotFound unless p
              instance_variable_set("@" + parent_name, p)
            end
          end

          define_method :_get_model do
            p = instance_variable_get("@" + belongs[-1].to_s.underscore.singularize)
            p.method(single ? model.model_name.singular : model.model_name.plural).call
          end
          before_filter :_load_parents
        end
      else
        define_method :_get_model do
          model
        end
      end

      if single
        define_method :_load_object do
          return if instance_variable_get("@" + name).present?
          mod = _get_model
          p = options[:collection]
          if p
            if p.kind_of?(Symbol)
              mod = method(p).call(mod)
            elsif p.arity == 1
              mod = p.call(mod)
            else
              mod = p.call(self, mod)
            end
          end
          mod = mod.first if mod.is_a?(Class) || mod.is_a?(ActiveRecord::Relation)
          instance_variable_set("@" + name, mod)
        end
        before_filter :_load_object, except: [:index, :new]
      else
        define_method :_load_object do
          return if instance_variable_get("@" + name).present?
          if params[:id]
            m = _get_model
            if options[:key]
              key = options[:key]
              p = m.find_by((key.is_a?(Proc) ? key.call(params[:id], self) : key).to_s + "=?", params[:id])
            else
              if m.respond_to?(:friendly)
                p = m.friendly.find(params[:id])
              else
                p = m.by_key(params[:id]).first
              end
            end
            raise ActiveRecord::RecordNotFound unless p
            instance_variable_set("@" + name, p)
          end
        end
        before_filter :_load_object, except: [:index, :new]
      end

      # index
      if methods.include?(:index)
        define_method :index do
          if respond_to?(:before_index)
            before_index
            return if response_body
          end
          respond_to do |format|
            format.json do
              mod = _get_model
              mod = mod.where(kind: params[:kind]) if params[:kind]
              mod = mod.where.not(kind: params[:exceptkind]) if params[:exceptkind]

              # reorder
              if has_weight and params[:reorder]
                from = params[:fromPosition].to_i
                to = params[:toPosition].to_i
                model.transaction do
                  #m = mod.find_by_weight(from)
                  m = mod.where(weight_field_name => from).first
                  if from < to
                    mod.where("#{weight_field_name} > %i And #{weight_field_name} <= %i" %[from, to]).update_all("#{weight_field_name} = #{weight_field_name} - 1")
                  else
                    mod.where("#{weight_field_name} >= %i And #{weight_field_name} < %i" %[to, from]).update_all("#{weight_field_name} = #{weight_field_name} + 1")
                  end
                  m[weight_field_name] = to
                  m.save
                end
                return render json: {}
              end

              # toggle
              if has_toggle and params[:toggle]
                m = model.find(params[:id])
                if _get_max_active < 0 or m.active or (mod.where(active: true).where.not(id: m.id).count < _get_max_active) then
                  m.update(active: !m.active)
                  return render json: m.valid? ? m.active : nil
                else
                  return render json: m.valid? ? {active: m.active, error: "You may have up to " + _get_max_active.to_s + " active items at a time!"} : nil
                end
              end

              # datatables
              if datatables and params["iDisplayStart"]
                # collection
                #mod = (datatables[:collection] || options[:collection]).call(mod) if datatables[:collection] || options[:collection]
                p = datatables[:filter]
                if p
                  if p.kind_of?(Symbol)
                    mod = method(p).call(mod)
                  elsif p.arity == 1
                    mod = p.call(mod)
                  else
                    mod = p.call(self, mod)
                  end
                end

                offset = params["iDisplayStart"].to_i
                limit = params["iDisplayLength"].to_i
                limit = 1000 if limit < 0
                total = mod.count
                display = total

                tbl = mod.table_name + "."
                order = datatables[:order]
                if !order and has_weight and params[:with_weight]
                  order = [tbl + weight_field_name, tbl + "id"]
                  if mod.group(tbl + weight_field_name).having("count(*) > 1").exists?
                    model.transaction do
                      mod.select(tbl + "id," + tbl + weight_field_name).order(tbl + weight_field_name + "," + tbl + "id").each_with_index {|m, i| m.update_column(weight_field_name.to_sym, i + 1) if m[weight_field_name] != i + 1}
                    end
                  end
                end

                if !has_weight or params[:no_weight] or !params[:with_weight]
                  order = []
                  (0...params[:iSortingCols].to_i).each do |i|
                    col = params["iSortCol_#{i}"]
                    if col =~ /^\d+$/
                      col = (col.to_i + 1).to_s
                    else
                      col = col.gsub(/[^a-zA-Z0-9_]/, '')
                    end
                    order << col + " " + params["sSortDir_#{i}"]
                  end
                  order = order.join(",")
                end

                mod = mod.select("#{tbl}#{weight_field_name} as _weight") if has_weight && params[:with_weight]

                # collection
                #mod = (datatables[:collection] || options[:collection]).call(mod) if datatables[:collection] || options[:collection]
                p = datatables[:collection] || options[:collection]
                if p
                  if p.kind_of?(Symbol)
                    mod = method(p).call(mod)
                  elsif p.arity == 1
                    mod = p.call(mod)
                  else
                    mod = p.call(self, mod)
                  end
                end

                # search
                search = params[:sSearch]
                if datatables[:search] and search
                  search.strip!
                  unless search.empty?
                    if datatables[:search].is_a?(String)
                      cond = datatables[:search]
                    else
                      #cond = "lower(" + Array(datatables[:search]).join(" || ") + ") like ?" # slite3 version
                      cond = "lower(concat(" + Array(datatables[:search]).collect{|c| "coalesce(#{c},'')"}.join(",") + ")) like ?"
                    end
                    mod = mod.where(cond, "%" + search.downcase + "%") unless search.empty?
                    display = mod.except(:select).select("*").count
                  end
                end

                # fields
                attachments = {}
                t = model
                while t && t.respond_to?(:attachment_definitions) do
                  attachments.merge!(t.attachment_definitions)
                  t = t.superclass
                end
                fields = []
                fields = datatables[:fields].dup if datatables[:fields] && !datatables[:fields].kind_of?(Proc)
                if fields.is_a?(Array)
                  fields.each_with_index do |field, index|
                    fields[index] = field.to_s + "_file_name" if attachments.has_key?(field)
                  end
                end
                mod = mod.select(fields + (fields.is_a?(Array) ? ["#{tbl}id"] : ",#{tbl}id"))
                mod = mod.select("#{tbl}active as _active") if has_toggle

                proc = datatables[:fields].kind_of?(Proc) ? datatables[:fields] : datatables[:row]
                aaData = mod.offset(offset).limit(limit).order(order).collect do |row|
                  res = {"DT_RowId" => row.id, "id" => row.id}
                  res["active"] = row._active if has_toggle
                  index = 0
                  row.attribute_names.each do |field|
                    if field.ends_with?("_file_name") and attachments.has_key?(f = field.gsub(/_file_name$/, '').to_sym)
                      t = attachments[f]
                      style = t.has_key?(:index_style) ? t[:index_style] : nil
                      style = :thumb if t[:styles] && t[:styles].kind_of?(Hash) && t[:styles].has_key?(:thumb)
                      res[index] = row.method(f).call.url(style)
                      res[f] = res[index]
                    else
                      res[field] = row[field] if !row[field].nil? && !['DT_RowId', 'id', 'active'].include?(field) && (field.length > 2 || field !~ /^\d+$/)
                      res[index] = row[field]
                    end
                    index+= 1
                  end
                  if proc
                    r = proc.arity == 1 ? proc.call(row) : (proc.arity == 3 ? proc.call(row, res, self) : proc.call(row, res))
                    if r.kind_of?(Hash)
                      r.each{|k, v| res[k] = v}
                    elsif r.kind_of?(Array)
                      if datatables[:fields].kind_of?(Proc)
                        index = has_weight && params[:with_weight] ? 1 : 0
                      end
                      r.each_with_index{|v, i| res[(i + index).to_s] = v}
                    end
                  end
                  if params[:include_settings] && row.respond_to?(:settings)
                    params[:include_settings].split(",").each do |key|
                      res[key] = row.settings[key]
                    end
                  end
                  res
                end

                render json: {
                  max_active: _get_max_active,
                  sEcho: params["sEcho"],
                  iTotalRecords: total,
                  iTotalDisplayRecords: display,
                  aaData: aaData
                }
              end
            end
            format.html
          end
        end
      end

      # new
      if methods.include?(:new)
        define_method :new do
          if respond_to?(:before_new)
            before_new
            return if response_body
          end
          instance_variable_set("@" + name, _get_model.new)
          if respond_to?(:after_new)
            after_new
            return if response_body
          end
          render template_exists?("new", params[:controller]) ? :new : (template_exists?("edit", params[:controller]) ? :edit : :show)
        end
      end

      # create
      if methods.include?(:create)
        define_method :create do
          # jquery.sortable
          if has_weight && request.xhr? && params[:sortable_item] && params[:sortable_item].is_a?(Array)
            m = _get_model
            weight = 1
            m.transaction do
              params[:sortable_item].each do |item|
                p = m.find(item)
                p[weight_field_name] = weight
                weight+= 1
                p.save
              end
            end
            render text: "{}"
            return
          end

          m = _get_model.new(_params)
          instance_variable_set("@" + name, m)
          
          if respond_to?(:before_create)
            before_create
            return if response_body
          end

          if respond_to?(:before_save)
            before_save
            return if response_body
          end

          if m.valid?
            if has_weight
              if (_get_new_records_at || :top) == :top
                # add new to top
                m[weight_field_name] = 1
                _get_model.update_all("#{weight_field_name} = #{weight_field_name} + 1")
              else
                # add new to botom
                m[weight_field_name] = (_get_model.maximum(weight_field_name) || 0) + 1
              end
            end
            if has_toggle and _get_max_active >= 0 and _get_model.where(active: true).count >= _get_max_active
              m.active = false
            end
          end

          if m.save
            # settings
            if m.respond_to?(:settings) and params[name.to_sym] and (params[name.to_sym][:settings] or params[name.to_sym][:rails_settings_scoped_settings])
              (params[name.to_sym][:settings] or params[name.to_sym][:rails_settings_scoped_settings]).each_entry {|k, v| m.settings[k] = v}
              params[name.to_sym].delete(:rails_settings_scoped_settings)
            end
            # subnodes & images
            params[name.to_sym].each do |k, v|
              if k.start_with?("image.") and v.is_a?(Hash)
                image = Node.new(v)
                image.kind = :image
                image.un = k[6..-1]
                m.nodes << image
              end
              if k.start_with?("node.") and v.is_a?(Hash)
                node = Node.new(v)
                node.un = k[5..-1]
                node.name||= node.un
                m.nodes << node
              end
            end

            if respond_to?(:after_save)
              after_save
              return if response_body
            end
            respond_to do |format|
              format.json do
                render json: m.to_json
              end
              format.html do
                flash[:notice] = notice_message
                redirect_to action: _method_for_action("index") ? :index : :new
              end
            end
          else
            respond_to do |format|
              format.json do
                render json: m.to_json
              end
              format.html do
                render action: template_exists?("edit", params[:controller]) ? :edit : :show
              end
            end
          end
        end
      end

      # show
      if methods.include?(:show)
        define_method :show do
          if respond_to?(:before_show)
            before_show
            return if response_body
          end
          render "show"
        end
      end

      # edit
      if methods.include?(:edit)
        define_method :edit do
          if respond_to?(:before_edit)
            before_edit
            return if response_body
          end
          render template_exists?("edit", params[:controller]) ? :edit : :show
        end
      end

      # update
      if methods.include?(:update)
        define_method :update do
          m = instance_variable_get("@" + name)
          if respond_to?(:before_update)
            before_update
            return if response_body
          end
          if respond_to?(:before_save)
            before_save
            return if response_body
          end
          if m.valid?
            if has_toggle and m.active and _get_max_active >= 0 and _get_model.where(active: true).where.not(id: m.id).count >= _get_max_active
              m.active = false
            end
          end
          
          result = m.update_attributes(_params)
          
          # destroy image if removed
          if _params["image"] == ''
            m.image = nil
            m.save
          end
          
          if result
            # settings
            if m.respond_to?(:settings) and params[name.to_sym] and (params[name.to_sym][:settings] or params[name.to_sym][:rails_settings_scoped_settings])
              (params[name.to_sym][:settings] or params[name.to_sym][:rails_settings_scoped_settings]).each_entry {|k, v| m.settings[k] = v}
              params[name.to_sym].delete(:rails_settings_scoped_settings)
            end
            # images && nodes
            params[name.to_sym].each do |k, v|
              if k == 'nodes_attributes' and v["0"]["image"] == ''
                image = m.nodes.find_by_kind_and_name(v["0"]["kind"], v["0"]["name"])
                if image
                  image.image = nil
                  image.save
                end
                next
              end
              if k.start_with?("image.") and v.is_a?(Hash)
                image_name = k[6..-1]
                image = m.nodes.find_by_kind_and_un(:image, image_name)
                image = m.nodes.build(un: image_name, kind: :image) unless image
                image.name||= image.un
                image.update(v)
                if v['image'] == ''
                  image.image = nil
                  image.save
                end
              end
              if k.start_with?("node.") and v.is_a?(Hash)
                node_name = k[5..-1]
                node = m.nodes.find_by_name(node_name)
                node = m.nodes.build(name: node_name) unless node
                node.name||= node.un
                node.update(v)
                if v['image'] == ''
                  node.image = nil
                  node.save
                end
              end
            end
          end

          if respond_to?(:after_save)
            after_save
            return if response_body
          end
          if result
            flash[:notice] = notice_message
            redirect_to action: _method_for_action("index") ? :index : (_method_for_action("edit") ? :edit : :show)
          else
            render action: template_exists?("edit", params[:controller]) ? :edit : :show
          end
        end
      end

      # destroy
      if methods.include?(:destroy)
        define_method :destroy do
          m = instance_variable_get("@" + name)
          if respond_to?(:before_destroy)
            before_destroy
            return if response_body
          end
          m.destroy if m
          if respond_to?(:after_destroy)
            after_destroy
            return if response_body
          end
          respond_to do |format|
            format.json do
              render json: m
            end
            format.html do
              redirect_to action: :index if _method_for_action("index")
            end
          end
        end
      end
      
      define_method :_method_for_action do |name|
        m = method_for_action(name)
        m && !m.to_s.start_with?("_handle")
      end

      define_method :_get_max_active do
        @max_active || max_active
      end

      define_method :_get_new_records_at do
        @new_records_at || new_records_at
      end
    end

  end
end

ActiveSupport.on_load(:action_controller) do
  extend AdminHub::Resources::ActionController
end

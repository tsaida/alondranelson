module AdminForms
  class FormBuilder < ::ActionView::Helpers::FormBuilder
    require_relative 'helpers/wrappers'
    include AdminForms::Helpers::Wrappers

    delegate :content_tag, :hidden_field_tag, :check_box_tag, :radio_button_tag, :button_tag, :link_to, :to => :@template

    def error_messages
      if object.try(:errors) and object.errors.full_messages.any?
        content_tag(:div, :class => 'alert alert-block alert-error validation-errors') do
          content_tag(:h4, I18n.t('admin_forms.errors.header', :model => object.class.model_name.human), :class => 'alert-heading') +
          content_tag(:ul) do
            object.errors.full_messages.map do |message|
              content_tag(:li, message)
            end.join('').html_safe
          end
        end
      else
        '' # return empty string
      end
    end

    %w(
      select
      collection_select
      country_select
      datetime_select
      date_select
      time_select
      time_zone_select

      email_field
      file_field
      number_field
      password_field
      phone_field
      range_field
      search_field
      telephone_field
      text_area
      text_field
      url_field
    ).each do |method_name|
      define_method(method_name) do |name, *raw_args, &proc|
        # Special case for select
        if method_name == 'select' or method_name == 'country_select'
          while raw_args.length < 3
            raw_args << {}
          end
        end

        options = {}
        html_options = {}

        if raw_args.length > 0
          if raw_args[-1].is_a?(Hash) && raw_args[-2].is_a?(Hash)
            html_options = raw_args[-1]
            options = raw_args[-2]
          elsif raw_args[-1].is_a?(Hash)
            options = raw_args[-1]
          end
        end

        # Add options hash to argument array if its empty
        raw_args << options if raw_args.length == 0
        if options.delete(:wrapper) == false
          options[:class]||= "form-control"
          html_options[:class]||= "form-control"
          super(name, *raw_args)
        else
          @name = name
          @field_options = field_options(options)
          @field_options[:class]||= "form-control"
          html_options[:class]||= "form-control"
          @args = options
          options.delete(:field_class)

          form_group_div do
            label_field + input_div do
              options.merge!(@field_options.merge(required_attribute))
              input_append = (options[:append] || options[:prepend] || options[:append_button]) ? true : nil
              res = extras(input_append) {super(name, *raw_args)}
              res = (res + content_tag("span", &proc)).html_safe if proc
              res
            end
          end
        end
      end
    end

    def field(name, *raw_args, &block)
      options = {}
      html_options = {}
      if raw_args.length > 0
        if raw_args[-1].is_a?(Hash) && raw_args[-2].is_a?(Hash)
          html_options = raw_args[-1]
          options = raw_args[-2]
        elsif raw_args[-1].is_a?(Hash)
          options = raw_args[-1]
        end
      end
      @name = name
      @field_options = field_options(options)
      @field_options[:class]||= "form-control"
      @args = options

      form_group_div do
        label_field + input_div do
          options.merge!(@field_options.merge(required_attribute))
          input_append = (options[:append] || options[:prepend] || options[:append_button]) ? true : nil
          extras(input_append) do
            yield
          end
        end
      end
    end

    def check_box(name, args = {}, checked_value = "1", unchecked_value = "0")
      @name = name
      @field_options = field_options(args)
      @args = args
      if @field_options.delete(:simple)
        return extras { super(name, @args.merge(@field_options), checked_value, unchecked_value) + (@field_options[:label].nil? ? human_attribute_name : @field_options[:label]) }
      end

      #form_group_div do
      #  input_div do
          @field_options.merge!(required_attribute)
          if @field_options[:label] == false || @field_options[:label] == ''
            extras { super(name, @args.merge(@field_options)) }
          else
            klasses = 'checkbox'
            klasses << ' inline' if @field_options.delete(:inline) == true
            @args.delete :inline
            label(@name, :class => klasses) do
              extras { super(name, @args.merge(@field_options), checked_value, unchecked_value) + (@field_options[:label].nil? ? human_attribute_name : @field_options[:label]) }
            end
          end
      #  end
      #end
    end

    def check_box_field(name, args = {})
      field(name, **args) do
        @field_options[:label]||= name
        options = @field_options.dup
        html = label(@name, class: "switch-light switch-jo", onclick: "$('input', this).blur()".html_safe) do
          check_box(name, **{class: "toggle", label: false}) +
          content_tag(:span) do
            content_tag(:span, options[:off] || "OFF") + 
            content_tag(:span, options[:on] || "ON")
          end +
          content_tag(:a)
        end
        @field_options = options
        html
      end
    end

    def switch_buttons(name, values = {}, opts = {})
      if values.length > 5
        return radio_buttons(name, values, opts)
      end
      @name = name
      @field_options = @options.slice(:namespace, :index).merge(opts.merge(required_attribute))
      form_group_div do
        label_field + input_div do
          content_tag(:div, class: "switch-toggle switch-#{values.length} switch-jo", onclick: "$('input', this).blur()".html_safe) do
            buttons = values.map do |text, value|
              radio_options = @field_options
              if value.is_a? Hash
                radio_options = radio_options.merge(value)
                value = radio_options.delete(:value)
              end
              radio_options[:class] = "toggle"

              radio_button(name, value, radio_options) +
              label("#{@name}_#{value}", text, onclick: "")
            end.join('')
            buttons << content_tag(:a)
            buttons << extras
            buttons.html_safe
          end
        end
      end
    end

    def radio_buttons(name, values = {}, opts = {})
      @name = name
      @field_options = @options.slice(:namespace, :index).merge(opts.merge(required_attribute))
      form_group_div do
        label_field + input_div do
          content_tag(:div, class: "radio-list") do
            klasses = 'radio'
            klasses << '-inline' if @field_options.delete(:inline) == true

            buttons = values.map do |text, value|
              radio_options = @field_options
              if value.is_a? Hash
                radio_options = radio_options.merge(value)
                value = radio_options.delete(:value)
              end

              label("#{@name}_#{value}", :class => klasses) do
                radio_button(name, value, radio_options) + text
              end
            end.join('')
            buttons << extras
            buttons.html_safe
          end
        end
      end
    end

    def collection_check_boxes(attribute, records, record_id, record_name, args = {})
      @name = attribute
      @field_options = field_options(args)
      @args = args

      form_group_div do
        label_field + input_div do
          options = @field_options.except(*ADMIN_OPTIONS).merge(required_attribute)
          # Since we're using check_box_tag() we may have to lookup the instance ourselves
          instance = object || @template.instance_variable_get("@#{object_name}")
          boxes = records.collect do |record|
            options[:id] = "#{object_name}_#{attribute}_#{record.send(record_id)}"
            checkbox = check_box_tag("#{object_name}[#{attribute}][]", record.send(record_id), [instance.send(attribute)].flatten.include?(record.send(record_id)), options)

            content_tag(:label, :class => ['checkbox', ('inline' if @field_options[:inline])].compact) do
              checkbox + record.send(record_name)
            end
          end.join('')
          boxes << extras
          boxes.html_safe
        end
      end
    end

    def collection_radio_buttons(attribute, records, record_id, record_name, args = {})
      @name = attribute
      @field_options = field_options(args)
      @args = args

      form_group_div do
        label_field + input_div do
          options = @field_options.merge(required_attribute)
          buttons = records.collect do |record|
            radiobutton = radio_button(attribute, record.send(record_id), options)
            content_tag(:label, :class => ['radio', ('inline' if @field_options[:inline])].compact) do
              radiobutton + record.send(record_name)
            end
          end.join('')
          buttons << extras
          buttons.html_safe
        end
      end
    end

    def uneditable_input(name, args = {})
      @name = name
      @field_options = field_options(args)
      @args = args

      form_group_div do
        label_field + input_div do
          extras do
            value = @field_options.delete(:value)
            @field_options[:class] = [@field_options[:class], 'uneditable-input'].compact

            content_tag(:span, @field_options) do
              value || object.send(@name.to_sym) rescue nil
            end
          end
        end
      end
    end

    def button(name = nil, args = {})
      name, args = nil, name if name.is_a?(Hash)
      @name = name
      @field_options = field_options(args)
      @args = args

      @field_options[:class] ||= 'btn'
      super(name, args.merge(@field_options))
    end

    def submit(name = nil, args = {})
      name, args = nil, name if name.is_a?(Hash)
      name = name || ("<i class='fa-check'></i> " + I18n.t('admin_forms.buttons.submit', default:'Submit')).html_safe
      @name = name
      @field_options = field_options(args)
      @args = args

      @field_options[:class] ||= 'btn btn-success'
      @field_options[:name]||= nil
      #super(name, args.merge(@field_options))
      button_tag(name, args.merge(@field_options))
    end

    def cancel(name = nil, args = {})
      name, args = nil, name if name.is_a?(Hash)
      name ||= I18n.t('admin_forms.buttons.cancel', default:'Cancel')
      @field_options = field_options(args)
      @field_options[:class] ||= 'btn btn-default cancel'
      @field_options[:back] ||= :back
      link_to(name, @field_options[:back], :class => @field_options[:class])
    end

    def actions(options={}, &block)
      options[:class]||= 'fluid'
      options[:offset] = 2 unless options[:offset] == false
      content_tag(:div, :class => 'form-actions ' + (options[:class] || '')) do
        if block_given?
          yield
        else
          buttons = [ submit(options[:name]), options[:cancel] != false ? cancel(options[:cancel].is_a?(String) ? options[:cancel] : nil, {class: options[:cancel_class], back: options[:back]}) : '' ].join(' ').html_safe
          if options[:offset]
            content_tag(:div, class: "col-md-offset-#{options[:offset]}") do
              buttons
            end
          else
            buttons
          end
        end
      end
    end

    def date_field(name, options={})
      field name, options do
        content_tag(:div, class: "input-group input-medium date") do
          options[:class] = (options[:class] || "") + " date-picker form-control"
          text_field(name, {wrapper: false}.merge(options)) +
          content_tag(:span, class: "input-group-btn", style: "vertical-align: top;") do
            content_tag(:span, class: "btn btn-info", type: "button", onclick: "$('input', $(this).parent().parent()).focus()") do
              "<i class='fa-calendar'></i>".html_safe
            end
          end
        end
      end
    end

    def datetime_field(name, options)
      field name, options do
        content_tag(:div, class: "input-group date form_meridian_datetime") do
          options[:class] = (options[:class] || '') + " datetime-picker"
          text_field(name, {wrapper: false, readonly: true}.merge(options)) +
          content_tag(:span, class: 'input-group-btn') do
            content_tag(:span, class: "btn btn-success date-reset", type: "button", onclick: "$('input', $(this).parent().parent()).focus()") do
              '<i class="fa fa-times"></i>'.html_safe
            end
            content_tag(:span, class: "btn btn-success date-set", type: "button", onclick: "$('input', $(this).parent().parent()).focus()") do
              '<i class="fa fa-calendar"></i>'.html_safe
            end
          end
        end
      end
    end

    def time_field(name, options)
      field name, options do
        content_tag(:div, class: "input-group bootstrap-timepicker") do
          options[:class] = (options[:class] || '') + " timepicker-default"
          text_field(name, {wrapper: false}.merge(options)) +
          content_tag(:span, class: 'input-group-btn') do
            content_tag(:span, class: "btn btn-default", type: "button", onclick: "$('input', $(this).parent().parent()).focus()") do
              '<i class="fa fa-clock-o"></i>'.html_safe
            end
          end
        end
      end
    end

    def image_upload(name, options={})
      retina = options.delete(:retina)
      retina = true if retina.nil?

      width = options[:width] || 0
      height = options[:height] || 0
      scale = (options[:scale] || 0.5).to_f
      
      if retina
        width = width * 2
        height = height * 2
      end

      unless options[:scale]
        scale = 665.0 / width if width * scale > 665
        scale = 400.0 / height if height * scale > 400
      end
      max = options.delete(:max)
      w = (width * scale).to_i
      h = (height * scale).to_i
      default_size = (300.0 / scale).to_i

      max_str = max ? "max " : ""
      if retina
        if width > 0 and height > 0
          options[:label_info] = "(%sretina size %ix%ipx, %sdefault size %ix%ipx)" % [max_str, width, height, max_str, width/2, height/2]
          size_text = "%ix%ipx" % [width, height]
        elsif width > 0 and height == 0
          options[:label_info] = "(%sretina width %ipx, %sdefault width %ipx)" % [max_str, width, max_str, width/2]
          size_text = "width+%ipx" % [width]
        elsif width == 0 and height > 0
          options[:label_info] = "(%sretina height %ipx, %sdefault height %ipx)" % [max_str, height, max_str, height/2]
          size_text = "height+%ipx" % [height]
        else
          size_text = "any+size"
        end
      else
        if width > 0 and height > 0
          options[:label_info] = "(%ssize %ix%ipx)" % [max_str, width, height]
          size_text = "%ix%ipx" % [width, height]
        elsif width > 0 and height == 0
          options[:label_info] = "(%swidth %ipx)" % [max_str, width, height]
          size_text = "width+%ipx" % [width]
        elsif width == 0 and height > 0
          options[:label_info] = "(%sheight %ipx)" % [max_str, height]
          size_text = "height+%ipx" % [height]
        else
          size_text = "any+size"
        end
      end

      path = options[:path]
      if path
        url = path
        has_attachment = File.file?(Rails.root.join("public", path))
      else
        image = object.method(name).call
        has_attachment = !object.new_record? && !image.blank?
        removeable = !options[:required] && !object.class.validators_on(name).any? {|v| (v.kind_of?(ActiveModel::Validations::PresenceValidator) || v.kind_of?(Paperclip::Validators::AttachmentPresenceValidator)) && !conditional_validators?(v)}
        url = image.url
      end
      removeable = options[:removeable] if options.has_key?(:removeable)
      options.delete(:required) if !removeable && has_attachment && options[:required]

      options[:field_class]||= "col-md-9"
      field(name, options) do
        if !@field_options.has_key?(:help_inline) && @field_options[:label]
          @field_options[:help_inline] = "Upload #{@field_options[:label]}. "
          if max
            if width > 0 and height > 0
              @field_options[:help_inline] << "It will be resized to maximum %i x %i pixels" % [width, height]
              @field_options[:help_inline] << " for retina and %i x %i pixels for default" % [width / 2, height / 2] if retina
              @field_options[:help_inline] << "."
            elsif width > 0 and height == 0
              @field_options[:help_inline] << "It will be resized to maximum %i pixels wide" % [width]
              @field_options[:help_inline] << " for retina and maximum %i pixels wide for default" % [width / 2] if retina
              @field_options[:help_inline] << "."
            elsif width == 0 and height > 0
              @field_options[:help_inline] << "It will be resized to maximum %i pixels tall" % [height]
              @field_options[:help_inline] << " for retina and maximum %i pixels tall for default" % [height / 2] if retina
              @field_options[:help_inline] << "."
            end
          else
            if width > 0 and height > 0
              @field_options[:help_inline] << "It will be resized to %i x %i pixels" % [width, height]
              @field_options[:help_inline] << " for retina and %i x %i pixels for default" % [width / 2, height / 2] if retina
              @field_options[:help_inline] << "."
            elsif width > 0 and height == 0
              @field_options[:help_inline] << "It will be resized to %i pixels wide" % [width]
              @field_options[:help_inline] << " for retina and %i pixels wide for default" % [width / 2] if retina
              @field_options[:help_inline] << "."
            elsif width == 0 and height > 0
              @field_options[:help_inline] << "It will be resized to %i pixels tall" % [height]
              @field_options[:help_inline] << " for retina and %i pixels tall for default" % [height / 2] if retina
              @field_options[:help_inline] << "."
            end
          end
        end

        options[:help_inline] = "Upload "
        content_tag(:div, class: "fileinput fileinput-" + (has_attachment ? 'exists' : 'new'), "data-provides" => 'fileinput') do
          content_tag(:div, class: "fileinput-new thumbnail") do
            content_tag(:img, "", alt: "", width: w > 0 ? w : nil, height: h > 0 ? h : nil, src: "http://www.placehold.it/%ix%i/EFEFEF/AAAAAA&text=%s" % [width > 0 ? width : default_size, height > 0 ? height : default_size, size_text])
          end +
          content_tag(:div, "", class: "fileinput-preview fileinput-exists thumbnail", style: "max-width:#{w > 0 ? w : 600}px; max-height:#{h > 0 ? h : 600}px; line-height: 20px;") do
            #content_tag(:img, "", alt: "", src: url, width: w > 0 ? w : nil, height: h > 0 ? h : nil) if has_attachment
            content_tag(:img, "", alt: "", src: url) if has_attachment
          end +
          content_tag(:div) do
            content_tag(:span, class: 'btn btn-default btn-file') do
              opts = options.merge(wrapper: false, class: "default", "data-target" => path ? nil : "#{object.class.name}.#{name}.#{object.id}", "data-exists" => has_attachment ? true : nil)
              opts.delete("data-required") if has_attachment
              content_tag(:span, "<i class='fa-paperclip'></i> Select Image".html_safe, class: "fileinput-new") +
              content_tag(:span, "<i class='fa-paperclip'></i> Change".html_safe, class: "fileinput-exists") +
              file_field(name, opts)
            end + " &nbsp; ".html_safe +
            if removeable
              content_tag(:a, href: "#", class: 'btn btn-danger fileinput-exists', "data-dismiss" => 'fileinput') do
                content_tag(:i, "", class: "fa-trash-o") + " Remove"
              end
            else
              ""
            end
          end
        end + (
          if respond_to?(:object) and object.respond_to?(:errors) and object.errors[@name].present?
            content_tag(:span, "ERROR!", class: "label label-danger")
          else
            #content_tag(:span, "NOTE!", class: "label label-danger", style: "font-size:75%") +
            #content_tag(:small, " Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only")
          end
        )
      end
    end

    #
    # options:
    #  path - file path (optional)
    #
    def file_upload(name, options={})
      field(name, options) do
        file = object.method(name).call
        has_attachment = !object.new_record? && !file.blank?
        removeable = !object.class.validators_on(name).any? {|v| (v.kind_of?(ActiveModel::Validations::PresenceValidator) || v.kind_of?(Paperclip::Validators::AttachmentPresenceValidator)) && !conditional_validators?(v) }
        url = file.url

        content_tag(:div, class: 'fileinput fileinput-' + (has_attachment ? 'exists' : 'new'), "data-provides" => "fileinput") do
          content_tag(:div, class: "input-group input-xlarge") do
            content_tag(:div, class: "form-control uneditable-input span3", "data-trigger" => "fileinput") do
              content_tag(:i, "", class: "fa fa-file fileinput-exists") + "&nbsp; ".html_safe + content_tag(:span, has_attachment ? file.original_filename.gsub("_", " ") : "", class: "fileinput-filename", style: "line-height:22px")
            end +
            content_tag(:span, class: "input-group-addon btn default btn-file") do
              opts = options.merge(wrapper: false, class: "default", "data-target" => "#{object.class.name}.#{name}.#{object.id}", "data-exists" => has_attachment ? true : nil)
              opts.delete("data-required") if has_attachment

              content_tag(:span, "<i class='fa-paperclip'></i> Select file".html_safe, class: "fileinput-new") +
              content_tag(:span, "<i class='fa-paperclip'></i> Change".html_safe, class: "fileinput-exists") +
              file_field(name, opts)
            end +
            if has_attachment
              content_tag(:a, "Preview", href: url, target: '_blank', class: "input-group-addon btn fileinput-exists", style: "position:relative; z-index:10")
            else
              ""
            end +
            if removeable
              content_tag(:a, "Remove", href: "#", class: "input-group-addon btn btn-danger fileinput-exists", "data-dismiss" => "fileinput", style: "position:relative; z-index:10")
            else
              ""
            end
          end
        end
        #    content_tag(:span, class: 'btn btn-default btn-file') do
        #      opts = options.merge(wrapper: false, class: "default", "data-target" => "#{object.class.name}.#{name}.#{object.id}", "data-exists" => has_attachment ? true : nil)
        #      opts.delete("data-required") if has_attachment
        #      content_tag(:span, "<i class='fa-paperclip'></i> Select file".html_safe, class: "fileinput-new") +
        #      content_tag(:span, "<i class='fa-paperclip'></i> Change".html_safe, class: "fileinput-exists") +
        #      file_field(name, opts)
        #    end + " &nbsp; ".html_safe +
        #    content_tag(:span, has_attachment ? link_to(object["#{name}_file_name"], url, target: '_blank').html_safe : '', class: "fileinput-preview", style: "margin-left:5px") +
        #    if removeable
        #      content_tag(:a, "", href: "#", class: 'close fileinput-exists', style: "float: none; margin-left:5px", "data-dismiss" => 'fileinput')
        #    else
        #      ""
        #    end
        #  end
        #end
      end
    end

    def admin_fields_for(record_name, record_object = nil, options = {}, &block)
      options[:label_class]||= "col-md-2"
      options[:field_class]||= "col-md-6"
      options[:builder]||= AdminForms.default_form_builder
      fields_for(record_name, record_object, options, &block)
    end

    def validate(options={})
      return unless respond_to?(:object) and object.class.respond_to?('validators_on')
      opts = {}
      opts[:rules]||= {}
      tbl = object.class.model_name.singular
      attachments = {}
      t = object.class
      while t && t.respond_to?(:attachment_definitions) do
        t.attachment_definitions.each {|k, v| attachments[k] = v}
        t = t.superclass
      end
      attachments.each do |k, t|
        object.class.validators_on(k).each do |v|
          next unless valid_validator?(v) && !conditional_validators?(v)
          if v.kind_of?(ActiveModel::Validations::PresenceValidator) ||
            v.kind_of?(ActiveRecord::Validations::PresenceValidator) ||
            v.kind_of?(Paperclip::Validators::AttachmentPresenceValidator) ||
            v.options[:presence]
            if object.new_record? || !object.method(k).call.present?
              opts[:rules]["#{tbl}[#{k}]"] = {required: true}
            end
          end
        end
      end

      object.class.columns.each do |column|
        o = {}
        object.class.validators_on(column.name).each do |v|
          next if !valid_validator?(v) || conditional_validators?(v)
          if v.kind_of?(ActiveModel::Validations::PresenceValidator) ||
            v.kind_of?(ActiveRecord::Validations::PresenceValidator) ||
            #v.kind_of?(Paperclip::Validators::AttachmentPresenceValidator) ||
            v.options[:presence]
            # presence
            o[:required] = true
          end
          if v.kind_of?(ActiveModel::Validations::FormatValidator)
            # regexp
          elsif v.kind_of?(ActiveModel::Validations::LengthValidator)
            # length
            if v.options[:is]
              o[:rangelength] = [v.options[:is], v.options[:is]]
            elsif v.options[:minimum] && v.options[:maximum]
              o[:rangelength] = [v.options[:minimum], v.options[:maximum]]
            elsif v.options[:minimum]
              o[:minlength] = v.options[:minimum]
            elsif v.options[:maximum]
              o[:maxlength] = v.options[:maximum]
            end
          elsif v.kind_of?(ActiveModel::Validations::NumericalityValidator)
            # numericality
            o[:number] = true
          end
        end

        opts[:rules]["#{tbl}[#{column.name}]"] = o if o.present?
      end
      opts[:rules].merge!(options[:rules]) if options.has_key?(:rules)
      content_tag(:script, ('$("#' + self.options[:html][:id] + '").validate(' + opts.to_json + ');').html_safe)
      #content_tag(:script, ('(function() { var f=$("#' + self.options[:html][:id] + '"); var os=' + (opts || {}).to_json + '; $("input[required]",f).each(function(i, k) {if (os.rules[k.name]) os.rules[k.name].required = true; else os.rules[k.name] = {required: true};}); console.log(os); f.validate(os);})();').html_safe)
    end

  private
    def field_options(args)
      if @options
        @options.slice(:namespace, :index).merge(args)
      else
        args
      end
    end
  end
end

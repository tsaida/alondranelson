module AdminHub
  module AttachmentHelper
    #
    # class Model < ActiveRecord::Base
    #   has_image :image, required: true, index_style: :thumb, styles: {...}
    #   has_attachment :pdf, required: true
    # end
    #
    # additional parameters:
    #  required - validates presence
    #  index_style - style used in datatables request
    #
    def has_image(name, options = {})
      options[:type] = :image
      unless options[:default_style]
        if options[:styles].is_a?(Hash) and options[:styles].size == 1
          if options[:styles].has_key?(:big)
            options[:default_style] = :big
          elsif options[:styles].has_key?(:retina)
            options[:default_style] = :retina
          end
          options[:default_style] = options[:styles].keys[0]
        end
      end
      has_attachment name, options
    end

    def has_video(name, options = {})
      options[:type] = :video
      has_attachment name, options
    end
    
    def has_audio(name, options = {})
      options[:type] = :audio
      has_attachment name, options
    end

    def has_pdf(name, options = {})
      options[:type] = :pdf
      options[:attachment] = true unless options.has_key?(:attachment)
      has_attachment name, options
    end

    def has_attachment(name, options = {})
      type = options[:type] || :data
      
      if type == :pdf
        options[:path] = ":rails_root/public/pdf/:filename"
        options[:url] = "/pdf/:filename"
      else
        attachment_path = "#{table_name}/:attachment/:id/" + (type == :image ? ":style/" : "") + ":filename"
      end
      
      if ENV['OPENSHIFT_DATA_DIR'].present?
        options[:path]||= ":rails_root/public/system/#{attachment_path}"
        options[:url] ||= "/system/#{attachment_path}"
      elsif (Rails.env.production? || Rails.env.beta?) && !options[:path]
        options[:path]||= attachment_path
        options[:url] ||= attachment_path
        options[:s3_headers]||= { 'Content-Disposition' => "attachment" } if options[:attachment]
      else
        options[:path]||= ":rails_root/public/system/#{attachment_path}"
        options[:url] ||= "/system/#{attachment_path}"
      end

      has_attached_file name, options

      validate_options = {}
      #validate_options[:presence] = true if options[:required]
      validate_options[:content_type] = {content_type: /^image\//} if type == :image
      validate_options[:content_type] = {content_type: /^video\//} if type == :video
      validate_options[:content_type] = {content_type: /^audio\//} if type == :audio
      validate_options[:content_type] = {content_type: [
        "pdf/adobe", "binary/octet-stream", "application/octet-stream", "application/x-pdf", "application/x-octet-stream",
        "application/x-download", "application/download", "application/force-download", "application/pdf", "doesn/matter"
      ]} if type == :pdf

      validates_attachment name, validate_options if validate_options.length > 0
      validates_presence_of name if options[:required]
    end
  end
end

ActiveSupport.on_load(:active_record) do
  extend AdminHub::AttachmentHelper
end

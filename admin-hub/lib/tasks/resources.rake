# desc "Explaining what the task does"
# task :resources do
#   # Task goes here
# end

namespace :adminhub do
  desc "Generates AdminHub assets."
  task :assets do
    
  end

  #
  # recalculates pathes for all nodes
  #
  desc "Updates paths for all nodes."
  task :update_paths => :environment do
    Node.all.each do |node|
      node.path = nil
      node.save
    end
    #Node.where(node_id: nil).each do |node|
    #  node.path = nil
    #  node.save
    #end
    puts "Update done!"
  end
end

# -*- encoding: utf-8 -*-
# stub: admin-hub 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "admin-hub"
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Dilshod"]
  s.date = "2014-03-24"
  s.description = "AdminHub."
  s.email = ["tdilshod@gmail.com"]
  s.files = [
    "lib/admin-hub", "lib/admin-hub/admin_forms", "lib/admin-hub/admin_forms/engine.rb", "lib/admin-hub/admin_forms/form_builder.rb", "lib/admin-hub/admin_forms/helpers",
    "lib/admin-hub/admin_forms/helpers/form_helper.rb", "lib/admin-hub/admin_forms/helpers/form_tag_helper.rb", "lib/admin-hub/admin_forms/helpers/nested_form_helper.rb",
    "lib/admin-hub/admin_forms/helpers/wrappers.rb", "lib/admin-hub/admin_forms/helpers.rb", "lib/admin-hub/admin_forms.rb", "lib/admin-hub/misc", "lib/admin-hub/misc/attachment_helper.rb",
    "lib/admin-hub/misc/delete_attachment.rb", "lib/admin-hub/resources", "lib/admin-hub/resources/action_resources.rb", "lib/admin-hub/resources/active_record.rb", "lib/admin-hub/resources.rb",
    "lib/admin-hub/version.rb", "lib/admin-hub.rb",
    "lib/tasks", "lib/tasks/resources.rake",
    "lib/generators",
    "MIT-LICENSE", "Rakefile", "README.rdoc"
  ]
  s.homepage = "http://google.com/"
  s.require_paths = ["lib"]
  s.rubygems_version = "2.1.7"
  s.summary = "AdminHub."

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, ["~> 4.1.5"])
    else
      s.add_dependency(%q<rails>, ["~> 4.1.5"])
    end
  else
    s.add_dependency(%q<rails>, ["~> 4.1.5"])
  end
end
